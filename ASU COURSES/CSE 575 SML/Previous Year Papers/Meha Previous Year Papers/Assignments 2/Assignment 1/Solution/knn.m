clear;
clc;
[FileName, PathName] = uigetfile('*.*', 'Select Training File');
filepath=fullfile(PathName,FileName);
data= importdata(filepath);
xtrain = data.traindata;
ytrain = data.trainlabels;
xtest = data.testdata;
ytest = data.testlabels;
eval = data.evaldata;
 %%
 dist = zeros(size(xtrain,1));
for i=1:size(xtrain,1)
   for j=1:size(xtrain,1)
       dist(i,j) = cosineDistance(xtrain(i,:), xtrain(j,:));
   end
end

%%
k = input('Enter value of k: ');
errorcount =0;
for i=1:size(xtrain,1)
   [sortedArray, index] = sortrows(dist, i);
   sortedArray = sortedArray(1:k,:);
   index = index(1:k,:);
   
   count1=0;
   count2=0;
   for j=1:size(index,1)
      if (ytrain(index(j,1)) == 1)
          count1 =count1 + 1;
      else
         count2= count2+1; 
      end
         
      if(count1>count2)
         templabel = 1; 
      else
         templabel = 2; 
      end
   end
   if(templabel ~= ytrain(i,1))
       errorcount = errorcount +1;
   end
end

errorrate = errorcount/size(xtrain,1);
fprintf('error rate: %f', errorrate);