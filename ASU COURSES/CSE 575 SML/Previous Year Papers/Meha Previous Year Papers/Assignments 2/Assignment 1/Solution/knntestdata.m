clear;
clc;
[FileName, PathName] = uigetfile('*.*', 'Select Training File');
filepath=fullfile(PathName,FileName);
data= importdata(filepath);
xtrain = data.traindata;
ytrain = data.trainlabels;
xtest = data.testdata;
ytest = data.testlabels;
eval = data.evaldata;
 %%
 dist = zeros(size(xtest,1));
for i=1:size(xtest,1)
   for j=1:size(xtrain,1)
       dist(i,j) = cosineDistance(xtrain(j,:), xtest(i,:));
   end
end

%%
k = input('Enter value of k: ');
errorcount =0;
for i=1:size(xtest,1)
   [sortedArray, index] = sort(dist, 2);
   sortedArray = sortedArray(:,1:k);
   index = index(:,1:k);
   count1=0;
   count2=0;
   for j=1:size(index,2)
      if (ytrain(index(i,j)) == 1)
          count1 = count1 + 1;
      else
         count2 = count2 + 1; 
      end
         
      if(count1 > count2)
         templabel = 1; 
      else
         templabel = 2; 
      end
   end
   if(templabel ~= ytest(i,1))
       errorcount = errorcount +1;
   end
end

errorrate = errorcount/size(xtest,1);
fprintf('error rate: %f', errorrate);