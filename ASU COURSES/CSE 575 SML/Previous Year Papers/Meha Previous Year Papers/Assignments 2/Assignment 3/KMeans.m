%% Clear Memory & Command Window
clc
clear all
close all

%% Prompt user fr input data and read the file
[FileName, PathName] = uigetfile('*.txt', 'Select Seed Data File');
filepath=fullfile(PathName,FileName);
input_data= importdata(filepath);
input_data = input_data(:,1:7);
[rows, columns] = size(input_data);

%% Perform k means clustering for different values of k

    k = input('Enter Number of clusters \n');
    a = 1;
    b = rows+1;
    sse=zeros(k,columns);
    % Generate random points as initial centroids
    r = fix((b-a).*rand(k,1) + a);
    centroids = zeros(k, columns);
    for i = 1:k
        centroids(i, :) = input_data(r(i),:);
    end;
    
    %n = 1000 is the maximum number of iterations of centroid updates 
    %if convergence is not reached
    for n = 1:1000
        % calculate Euclidean distance between each point and each centroid
        dist = zeros(rows, k);
        distance = zeros(rows,1);
        index = zeros(rows,1);
        for j = 1:rows 
            for i = 1:k
                dist(j,i) = pdist2(input_data(j,:),centroids(i,:),'euclidean');
            end;
            [distance(j), index(j)] = min(dist(j,:));
        end;
        
        % Obtain counts of each cluster
        y = sort(index(:));
        p = find([numel(y);diff(y);numel(y)]);
        values = y(p(1:end-1));
        instances = diff(p);
        oldcentroids = centroids;
        oldsse = sse;
        %Calculate new centroid values
        for j =1:k
            C = zeros(instances(j),columns);
            x = 0;
            for i = 1:rows
                if index(i) ==j
                    x=x+1;
                    C( x,:) = input_data(i,:);
                
                end;
            end;
            centroids(j, :) = mean(C,1);
            summation=zeros(1,columns);
            for m=1:instances(j)
               deviation = power((centroids(j,:)- C(m,:)),2);
               summation = summation + deviation;
            end
            sse(j,:) = summation;
        end;
        totalOldSSE = sum(sum(oldsse,2));
        totalNewSSE = sum(sum(sse,2));
        flag = true;
            if abs(totalNewSSE - totalOldSSE) > 0.001
                flag = false;
            end;
        if flag == true
            fprintf('Final SSE is: %f', totalNewSSE);
            break;           
        end;  
    end;
    