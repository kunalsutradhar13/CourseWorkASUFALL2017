% to predcit the test data set. 
function res = predict(Xtest, w)
nTest = size(Xtest,1);
res = zeros(nTest,1);
for i = 1:nTest
    sigVal = Sigmoid([1.0 Xtest(i,:)] * w);
    %assigning the label.
    if sigVal >= 0.5
        res(i) = 1;
    else
        res(i) = 0;
    end
end
end