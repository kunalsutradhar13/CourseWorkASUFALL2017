% to clear 
clear; clc;
%getting data file. 
[FileName, PathName] = uigetfile('*.*', 'Select Training File');
filepath=fullfile(PathName,FileName);
data= importdata(filepath);
%randomizing the data set
rand_values = randperm(size(data,1));
for i=1:size(data,1)
   temp = data(i,:);
   data(i,:) = data(rand_values(i),:);
   data(rand_values(i),:) = temp;
end
%changing the labels.
Xdata = data(:,2:end-1);
Ydata = data(:, end:end);
for i=1:size(Ydata,1)
   if (Ydata(i,1)== 2)
       Ydata(i,1) = 1;
   else
       Ydata(i,1)= 0;
   end
end
%putting the data into training and test.
Xtrain = Xdata(1:450,:);
Ytrain = Ydata(1:450,:);
Xtest = Xdata(451:end,:);
Ytest = Ydata(451:end,:);
[rows,columns] = size(Xtrain);
Pclass1 = zeros(10,9);
Pclass0 = zeros(10,9);
%prior probabilities. 
priorP1 = sum(Ytrain)/450;
priorP0 = 1- sum(Ytrain)/450;
% probabilities of each feature given class.
for i=1:columns
   for j=1:size(Ytrain,1)
      if(Ytrain(j,1) == 1)
         Pclass1(Xtrain(j,i),i)=Pclass1(Xtrain(j,i),i)+1; 
      elseif (Ytrain(j,1) == 0)
         Pclass0(Xtrain(j,i),i)=Pclass0(Xtrain(j,i),i)+1;      
      end
   end
end
count=0;
%predicting for the test data set.
for i = 1:size(Ytest,1)
   ProbClass1 = 1;
   ProbClass0 = 1;
   for j=1:9
      ProbClass1 = ProbClass1 * Pclass1(Xtest(i,j),j)/priorP1;
      ProbClass0 = ProbClass0 * Pclass0(Xtest(i,j),j)/priorP0;
   end
   if ((ProbClass1 > ProbClass0) && Ytest(i,1) == 1)
       count = count + 1;
   elseif((ProbClass1 < ProbClass0) && Ytest(i,1) == 0)
        count = count + 1;
   end
end
%displaying the percentage. 
per = count/size(Ytest,1);
disp(per);