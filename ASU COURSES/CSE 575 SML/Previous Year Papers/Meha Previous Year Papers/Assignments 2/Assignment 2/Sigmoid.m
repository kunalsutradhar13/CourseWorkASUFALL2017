% the sigmoid function.
function sig = Sigmoid(z)
sig = zeros(size(z));
sig = 1./(1 + exp(-z));
end