function [w] = Weights( Xtrain, Ytrain, w0, maxIter, learningRate)
[nSamples, nFeature] = size(Xtrain);
w = w0;
precost = 0;
for j = 1:maxIter
    temp = zeros(nFeature + 1,1);
    for k = 1:nSamples
        temp = temp + (Sigmoid([1.0 Xtrain(k,:)] * w) - Ytrain(k)) * [1.0 Xtrain(k,:)]';
    end
    w = w - learningRate * temp;
    cost = CostFunction(Xtrain, Ytrain, w);
    if j~=0 && abs(cost - precost) / cost <= 0.0001
        break;
    end
    precost = cost;

end
end