% the cost function
function [ J ] = CostFunction( Xtrain, Ytrain, w )
[nSamples, nFeature] = size(Xtrain);
temp = 0.0;
for m = 1:nSamples
    hx = Sigmoid([1.0 Xtrain(m,:)] * w);
    %updating weights
    if Ytrain(m) == 1
        temp = temp + log(hx);
    else
        temp = temp + log(1 - hx);
    end
end
J = temp / (-nSamples);
end