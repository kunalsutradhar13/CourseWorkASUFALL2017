% to clear
clear;
clc;
%getting file.
[FileName, PathName] = uigetfile('*.*', 'Select Training File');
filepath=fullfile(PathName,FileName);
data= importdata(filepath);
% to randomize the data set.
rand_values = randperm(size(data,1));
for i=1:size(data,1)
   temp = data(i,:);
   data(i,:) = data(rand_values(i),:);
   data(rand_values(i),:) = temp;
end
Xdata = data(:,2:end-1);
Ydata = data(:, end:end);
for i=1:size(Ydata,1)
   if (Ydata(i,1)== 2)
       Ydata(i,1) = 1;
   else
       Ydata(i,1)= 0;
   end
end
% putting them in training and test data sets
Xtrain = Xdata(1:450,:);
Ytrain = Ydata(1:450,:);
Xtest = Xdata(451:end,:);
Ytest = Ydata(451:end,:);
[nsamples, nfeatures] = size(Xdata);
w0 = rand(nfeatures + 1, 1);
%to calculate the weights.
weight = Weights( Xtrain, Ytrain, w0, 10000, 0.1);
%to predict the resuls
res = predict( Xtest, weight );
count =0;
for i=1:size(res,1)
   if(Ytest(i,1) ~= res(i,1))
       % counting errors
       count = count + 1;
   end
end
%displaying the percentage.
per = 1 - count/size(Xtest,1);
disp(per);