import csv
import numpy as numpyVar
import matplotlib.pyplot as plotLibVar
import plotly as py
import plotly.graph_objs as go
from numpy import array
varList1 = []
varList2 = []
with open('SkillCraft1_Dataset.csv') as csvFileInput:
	listOfDict = csv.DictReader(csvFileInput)
	for dictionary in listOfDict:
		varList1.append(float(dictionary['NumberOfPACs']))
		varList2.append(float(dictionary['GapBetweenPACs']))
# print varList1
# print varList2
valArray1 = numpyVar.asarray(varList1)
valArray2 = numpyVar.asarray(varList2)
trace1 = go.Scatter(
    x = valArray1,
    y = valArray2,
    mode = 'markers'
)
data = [trace1]
py.offline.plot(data, filename = 'NumberOfPACs_vs_GapBetweenPACs')

# import numpy as numpyVar
# import plotly.plotly as py
# import plotly.graph_objs as go
# SelectByHotkeys = numpyVar.genfromtxt('SkillCraft1_Dataset.csv', delimiter=',', usecols=6, dtype=float, names=True)
# AssignToHotkeys = numpyVar.genfromtxt('SkillCraft1_Dataset.csv', delimiter=',', usecols=7, dtype=float, names=True)
# trace1 = go.Scatter(
#     x = SelectByHotkeys,
#     y = AssignToHotkeys,
#     mode = 'markers'
# )
# data = [trace1]
# py.iplot(data, filename = 'basic-scatter')
# valArray = numpyVar.genfromtxt('SkillCraft1_Dataset.csv', delimiter=',', usecols=14, dtype=float, names=True)
# plotLibVar.hist(valArray.astype('float'))
# plotLibVar.title("ActionsInPAC Histogram")
# plotLibVar.xlabel("ActionsInPAC Values")
# plotLibVar.ylabel("Frequency")
# plotLibVar.show()


