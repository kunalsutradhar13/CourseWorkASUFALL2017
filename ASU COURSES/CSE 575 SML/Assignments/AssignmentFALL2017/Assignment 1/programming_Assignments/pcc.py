import csv
import numpy as numpyVar
import matplotlib.pyplot as plotLibVar
import plotly as py
import plotly.graph_objs as go
from numpy import array
varList = []
varnames = []

varList1 = []
varList2 = []
varList3 = []
varList4 = []
varList5 = []
varList6 = []
varList7 = []
varList8 = []
varList9 = []
varList10 = []
varList11 = []
varList12 = []
varList13 = []
varList14 = []
varList15 = []
varListmin1 = []
varListmin2 = []

with open('SkillCraft1_Dataset.csv') as csvFileInput:
	reader = csv.reader(csvFileInput)
 	row1 = next(reader)
 	#print type(row1)
 	#print row1

hashMap = {}
hashMap.update({'0': 'APM' })
hashMap.update({'1': 'SelectByHotkeys' })
hashMap.update({'2': 'AssignToHotkeys' })
hashMap.update({'3': 'UniqueHotkeys' })
hashMap.update({'4': 'MinimapAttacks' })
hashMap.update({'5': 'MinimapRightClicks' })
hashMap.update({'6': 'NumberOfPACs' })
hashMap.update({'7': 'GapBetweenPACs' })
hashMap.update({'8': 'ActionLatency' })
hashMap.update({'9': 'ActionsInPAC' })
hashMap.update({'10': 'ActionsInPAC' })
hashMap.update({'11': 'TotalMapExplored' })
hashMap.update({'12': 'WorkersMade' })
hashMap.update({'13': 'UniqueUnitsMade' })
hashMap.update({'14': 'ComplexUnitsMade' })
hashMap.update({'15': 'ComplexAbilitiesUsed' })

print hashMap

i=6
with open('SkillCraft1_Dataset.csv') as csvFileInput:
	listOfDict = csv.DictReader(csvFileInput)
	
	print type(listOfDict)
	for dictionary in listOfDict:
		varList1.append(float(dictionary['APM']))
		varList2.append(float(dictionary['SelectByHotkeys']))
		varList3.append(float(dictionary['AssignToHotkeys']))
		varList4.append(float(dictionary['UniqueHotkeys']))
		varList5.append(float(dictionary['MinimapAttacks']))
		varList6.append(float(dictionary['MinimapRightClicks']))
		varList7.append(float(dictionary['NumberOfPACs']))
		varList8.append(float(dictionary['GapBetweenPACs']))
		varList9.append(float(dictionary['ActionLatency']))
		varList10.append(float(dictionary['ActionsInPAC']))
		varList11.append(float(dictionary['TotalMapExplored']))
		varList12.append(float(dictionary['WorkersMade']))
		varList13.append(float(dictionary['UniqueUnitsMade']))
		varList14.append(float(dictionary['ComplexUnitsMade']))
		varList15.append(float(dictionary['ComplexAbilitiesUsed']))

valArray1 = numpyVar.asarray(varList1)
valArray2 = numpyVar.asarray(varList2)
valArray3 = numpyVar.asarray(varList3)
valArray4 = numpyVar.asarray(varList4)
valArray5 = numpyVar.asarray(varList5)
valArray6 = numpyVar.asarray(varList6)
valArray7 = numpyVar.asarray(varList7)
valArray8 = numpyVar.asarray(varList8)
valArray9 = numpyVar.asarray(varList9)
valArray10 = numpyVar.asarray(varList10)
valArray11 = numpyVar.asarray(varList11)
valArray12 = numpyVar.asarray(varList12)
valArray13 = numpyVar.asarray(varList13)
valArray14 = numpyVar.asarray(varList14)
valArray15 = numpyVar.asarray(varList15)
matrix = numpyVar.corrcoef([valArray1,valArray2,valArray3,valArray4,valArray5,valArray6,valArray7,valArray8,valArray9,valArray10,valArray11,valArray12,valArray13,valArray14,valArray15])
max = -1;
maxposi = 0
maxposj = 0
min = 1;
minposi = 0
minposj = 0 
for i,row in enumerate(matrix):
	for j, entry in enumerate(row):
		if(entry and entry > max and entry < 1):
			max= entry
			maxposi = i
			maxposj = j
		if(entry and entry < min):
			min = entry
			minposi = i
			minposj = j;

print matrix
numpyVar.savetxt("matrix.csv", matrix, delimiter=",")
print max
print min
print maxposi
print hashMap.get(str(maxposi))
print maxposj
print hashMap.get(str(maxposj))
print minposi
print hashMap.get(str(minposi))
print minposj
print hashMap.get(str(minposj))

with open('SkillCraft1_Dataset.csv') as csvFileInput:
	listOfDict = csv.DictReader(csvFileInput)
	for dictionary in listOfDict:
		varList1.append(float(dictionary[hashMap.get(str(maxposi))]))
		varList2.append(float(dictionary[hashMap.get(str(maxposj))]))
valArray1 = numpyVar.asarray(varList1)
valArray2 = numpyVar.asarray(varList2)
trace1 = go.Scatter(
    x = valArray1,
    y = valArray2,
    mode = 'markers'
)
data = [trace1]
py.offline.plot(data, filename = 'maxivsmaxj_scatter')


with open('SkillCraft1_Dataset.csv') as csvFileInput:
	listOfDict = csv.DictReader(csvFileInput)
	for dictionary in listOfDict:
		varListmin1.append(float(dictionary[hashMap.get(str(minposi))]))
		varListmin2.append(float(dictionary[hashMap.get(str(minposj))]))
valArray3 = numpyVar.asarray(varListmin1)
valArray4 = numpyVar.asarray(varListmin2)
trace2 = go.Scatter(
    x = valArray3,
    y = valArray4,
    mode = 'markers'
)
data = [trace2]
py.offline.plot(data, filename = 'minivsminj_scatter')







