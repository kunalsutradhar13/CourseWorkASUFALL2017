import plotly.plotly as py
import plotly.graph_objs as go
trace1 = go.Scatter(
	x=[1, 2, 3],
	y=[5, 6, 7],
	mode='markers',
	marker=Marker(
		color='red',
		symbol='square'
		)
	)
data = Data([trace1])
py.plot(data)