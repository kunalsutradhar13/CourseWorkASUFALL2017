import csv
import numpy as numpyVar
import matplotlib.pyplot as plotLibVar
from numpy import array
# varList = []
# with open('SkillCraft1_Dataset.csv') as csvFileInput:
# 	listOfDict = csv.DictReader(csvFileInput)
# 	for dictionary in listOfDict:
# 		varList.append(float(dictionary['ActionsInPAC']))
# #print varList
# valArray = numpyVar.asarray(varList)
valArray = numpyVar.genfromtxt('SkillCraft1_Dataset.csv', delimiter=',', usecols=14, dtype=float, names=True)
plotLibVar.hist(valArray.astype('float'))
plotLibVar.title("ActionsInPAC Histogram")
plotLibVar.xlabel("ActionsInPAC Values")
plotLibVar.ylabel("Frequency")
plotLibVar.show()
print varList