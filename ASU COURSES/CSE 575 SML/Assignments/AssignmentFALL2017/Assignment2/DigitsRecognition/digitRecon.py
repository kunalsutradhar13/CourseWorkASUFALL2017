
from mnist import MNIST
from sklearn import neighbors
import random
import datetime
import numpy as np

mndata = MNIST('train')
(images, labels) = mndata.load_training()
# index = random.randrange(1, len(images))
# print index
# print mndata.display(images[index])
# print type(images)
# print labels[index]
# print type(labels)

mndata1 = MNIST('test')
(images1, labels1) = mndata1.load_testing()
# index1 = random.randrange(1, len(images1))
# print index1
# print mndata1.display(images1[index1])
# print type(images1)
# print labels1[index1]
# print type(labels1)

startTime = datetime.datetime.now()
knn = neighbors.KNeighborsClassifier(n_neighbors=3)
knn.fit(images, labels)
match = 0
for i in xrange(len(images1)):
    a = np.array(images1[i]).reshape(1, -1)
    predictLabel = knn.predict(a)
    if predictLabel == labels1[i]:
        match += 1

print match
endTime = datetime.datetime.now()
print 'use time: ' + str(endTime - startTime)
print 'Accuracy rate: '+ str(match*1.0/len(images1)*100)

			