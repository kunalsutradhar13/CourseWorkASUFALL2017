from numpy import linalg as LA
from mnist import MNIST
from sklearn import neighbors
import random
import datetime
import numpy as np


def knn_x(data, x, k):
    n, m = data.shape

    # To calculate Euclidean distance efficiently,
    # use numpy subtraction/addition ops.
    x_data = np.tile(x, (n, 1))
    dist_matrix = LA.norm((x_data - data), axis=1)

    # Return an array containing the indexes k closest neighbors
    neighbors_indexes = np.argsort(dist_matrix)[:k]
    return neighbors_indexes


'''
Returns single prediction based on the majority vote of neighbor labels
'''


def knn_clf_x(neighbors, true_labels):
    votes = []
    for idx in neighbors:
        votes.append(true_labels[idx])
    votes = np.array(votes, dtype='int')
    return np.argmax(np.bincount(votes))


'''
K-nearest neighbors classifier implemented on test data.
'''


def knn_algorithm(train_data, train_labels, test_data, k):
    preds = []
    count = 0
    print len(test_data)

    for x in test_data:
        print count
        neighbors = knn_x(train_data, x, k)
        preds.append(knn_clf_x(neighbors, train_labels))
        count = count + 1
    return np.array(preds)


'''
Returns unweighted errors of the classifiers
'''


def calc_error(true_labels, pred_labels):
    assert true_labels.size == pred_labels.size, "Vectors not equal size"
    n = true_labels.size
    miscount = 0.0
    for i in xrange(n):
        if true_labels[i] != pred_labels[i]:
            miscount += 1.0
    return (miscount / n)


'''
Gets the training and validation error from the knn clf. 
'''


def main():
    f = open('output.txt', 'w')
    # train = np.genfromtxt('data/train_data.txt')
    # test = np.genfromtxt('data/test_data.txt')
    # val = np.genfromtxt('data/val_data.txt')

    mndata = MNIST('train')
    (images, labels) = mndata.load_training()
    index = random.randrange(1, len(images))
    print index
    print mndata.display(images[index])
    print type(images)
    print labels[index]
    print type(labels)

    mndata1 = MNIST('test')
    (images1, labels1) = mndata1.load_testing()
    index1 = random.randrange(1, len(images1))
    print index1
    print mndata1.display(images1[index1])
    print type(images1)
    print labels1[index1]
    print type(labels1)

    # Parse data for separating training labels and dataset
    # n_feat = train[0].size
    # train_data = train[:,:-1]
    # train_labels = train[:,n_feat-1]
    # test_data = test[:,:-1]
    # test_labels = test[:,n_feat-1]
    # val_data = val[:,:-1]
    # val_labels = val[:,n_feat-1]

    # Print training + validation error for the classifier
    # k_neighbors = [1,3,5,11,16,21]
    k_neighbors = [3]
    for k in k_neighbors:
        preds_train = knn_algorithm(np.array(images), labels, images, k)
        # preds_val = knn_algorithm(train_data,train_labels,val_data,k)
        preds_test = knn_algorithm(np.array(images1), labels, images1, k)
        f.write("%s-neighbors: \n" % k)
        f.write("Training error: %s \n" % (calc_error(labels, preds_train)))
        # f.write("Validation error: %s \n" % (calc_error(val_labels,preds_val)))
        f.write("Test error: %s \n" % (calc_error(labels1, preds_test)))
        f.write("\n")


if __name__ == '__main__':
    main()