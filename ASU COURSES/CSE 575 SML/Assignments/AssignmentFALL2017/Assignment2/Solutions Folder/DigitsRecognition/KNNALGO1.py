
import numpy as np
from numpy import linalg as LA
from mnist import MNIST
from sklearn import neighbors
import random
import datetime
import numpy as np

def knn_x(data, x, k):	
	n,m = data.shape
	x_data = np.tile(x,(n,1))	
	dist_matrix = LA.norm((x_data - data),axis=1)
	neighbors_indexes = np.argsort(dist_matrix)[:k]
	return neighbors_indexes

'''
Returns single prediction based on the majority vote of neighbor labels
'''
def knn_clf_x(neighbors, true_labels):
	votes = [] 
	for idx in neighbors: 
		votes.append(true_labels[idx])
	votes = np.array(votes,dtype ='int')
	return np.argmax(np.bincount(votes))

'''
K-nearest neighbors classifier implemented on test data.
'''	
def knn_algorithm(train_data,train_labels,test_data,k):
	preds = []
	count = 0
	#print len(test_data)

	for x in test_data:		
		#print count
		neighbors = knn_x(train_data,x,k)
		preds.append(knn_clf_x(neighbors,train_labels))
		count = count+1
		# print "preds : "
		# print preds
	return np.array(preds)

'''
Returns unweighted errors of the classifiers
'''
def calc_error(true_labels,pred_labels):
	assert true_labels.size == pred_labels.size, "Vectors not equal size"
	n = true_labels.size
	miscount = 0.0
	for i in xrange(n):
		if true_labels[i] != pred_labels[i]:
			miscount += 1.0
	return  (miscount/n)

'''
Gets the training and validation error from the knn clf. 
'''
def main():
	f = open('output.txt','w')
	mndata = MNIST('train')
	(images, labels) = mndata.load_training()
	mndata1 = MNIST('test')
	(images1, labels1) = mndata1.load_testing()
	
	startTime = datetime.datetime.now()
	k_neighbors = [1, 3, 5, 10, 30, 50, 70, 80, 90, 100]
	for k in k_neighbors:
		preds_test = knn_algorithm(np.array(images), labels,images1,k)
		f.write("Test Accuracy : %s \n" % (1-calc_error(labels1,preds_test)))
		f.write("\n")
	endTime = datetime.datetime.now()
	print 'use time: ' + str(endTime - startTime)

if __name__ == '__main__':
	main()