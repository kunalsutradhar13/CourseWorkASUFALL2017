import numpy as np
import random

np.random.seed(12)
num_observations = 4


def createDictOfData(filename):
    d = {}
    with open(filename) as data:
        count = 0
        for line in data:
            for word in line.split():
                if word not in d:
                    d[word] = list()
                    listValue = d[word]
                    for i in xrange(0, count, 1):
                        listValue.append(0)
                    listValue.insert(count, 1)
                else:
                    listValue = d[word]
                    try:
                        countLine = listValue[count]
                        countLine += 1;
                        listValue[count] = countLine
                    except IndexError:
                        while (len(listValue) < count):
                            listValue.append(0)
                        listValue.append(1)
                d[word] = listValue
            count = count + 1
    # print count
    for key in d:
        listValue = d[key]
        while (len(listValue) < count):
            listValue.append(0)
    return d


def getLabelList(filename):
    labelIndex = getLabelIndexList(filename)
    labelParentList = getLabelListFromParent()
    labelList = list()
    for i in xrange(0, len(labelIndex), 1):
        labelList.append(labelParentList[i])
    return labelList


def getSizeofData(filename):
    with open(filename) as data:
        train_count = 0
        for line in data:
            train_count += 1
    return train_count


def getLabelIndexList(filename):
    labelList = list()
    with open(filename) as data:
        for line in data:
            labelList.append(int(line))
    return labelList


def getLabelListFromParent():
    labelList = list()
    with open("farm-ads-label.txt") as test:
        for line in test:
            data = line.split()[1]
            labelList.append(int(data))
    return labelList


def sigmoid(scores):
    return 1 / (1 + np.exp(-scores))


def log_likelihood(features, target, weights):
    scores = np.dot(features, weights)
    ll = np.sum(target * scores - np.log(1 + np.exp(scores)))
    return ll


def logistic_regression(features, target, num_steps, learning_rate, add_intercept=False):
    if add_intercept:
        intercept = np.ones((features.shape[0], 1))
        features = np.hstack((intercept, features))


    weights = np.zeros(features.shape[1])

    for step in xrange(num_steps):
        scores = np.dot(features, weights)
        predictions = sigmoid(scores)

        output_error_signal = target - predictions

        gradient = np.dot(features.T, output_error_signal)
        weights += learning_rate * gradient

        if step % 10000 == 0:
            print log_likelihood(features, target, weights)

    return weights

def LogisticRegression(limit):
    with open("farm-ads.txt") as data:
        count = 0
        with open("test_data.txt", 'w') as test:
            with open("train_data.txt", 'w') as train:
                with open("test_label_index.txt", 'w') as test_label:
                    with open("train_label_index.txt", 'w') as train_label:
                        for line in data:
                            if random.random() < limit:
                                train.write(line)
                                train_label.write(str(count) + "\n")
                                count += 1
                            else:
                                test.write(line)
                                test_label.write(str(count) + "\n")
                                count += 1

    simulated_labels = np.asarray(getLabelList("train_label_index.txt"))
    print simulated_labels
    training = createDictOfData("train_data.txt")
    print "Got the dict "
    temp_list = list()
    final_list = list()
    for i in range(0, getSizeofData("train_data.txt")):
        for key in training:
            listTrain = training[key]
            temp_list.append(listTrain[i])
        final_list.append(temp_list)

    print "Got the words"
    simulated_labels = np.asarray(getLabelList("train_label_index.txt"))
    print "Got the feature labels"

    simulated_separableish_features = np.asarray(final_list)


    print "Got the featurelist"




    print "starting self LR"
    weights = logistic_regression(simulated_separableish_features, simulated_labels,
                                  num_steps=50, learning_rate=0.2, add_intercept=True)

    print weights
    data_with_intercept = np.hstack((np.ones((simulated_separableish_features.shape[0], 1)),
                                     simulated_separableish_features))

    final_scores = np.dot(np.asarray((np.ones((simulated_separableish_features.shape[0], 1)),
                                      simulated_separableish_features)), weights)
    preds = np.round(sigmoid(final_scores))

    print preds

    print 'Accuracy from scratch: {0}'.format((preds == simulated_labels).sum().astype(float) / len(preds))


LogisticRegression(0.1)
LogisticRegression(0.3)
LogisticRegression(0.5)
LogisticRegression(0.7)
LogisticRegression(0.8)
LogisticRegression(0.9)
