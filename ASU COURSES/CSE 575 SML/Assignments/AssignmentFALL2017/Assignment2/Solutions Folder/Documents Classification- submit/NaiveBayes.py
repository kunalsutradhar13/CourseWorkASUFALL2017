import random
import numpy as np
from collections import Counter, defaultdict
from sklearn.naive_bayes import GaussianNB

'''returns the length of lines in a data'''

def getSizeofData(filename):
    with open(filename) as data:
        train_count = 0
        for line in data:
            train_count += 1
    return train_count


'''prints the data matrix'''


def createDictOfData(filename):
    d = {}
    with open(filename) as data:
        count = 0
        for line in data:
            for word in line.split():
                if word not in d:
                    d[word] = list()
                    listValue = d[word]
                    for i in xrange(0, count, 1):
                        listValue.append(0)
                    listValue.insert(count, 1)
                else:
                    listValue = d[word]
                    try:
                        countLine = listValue[count]
                        countLine += 1;
                        listValue[count] = countLine
                    except IndexError:
                        while (len(listValue) < count):
                            listValue.append(0)
                        listValue.append(1)
                d[word] = listValue
            count = count + 1
    # print count
    for key in d:
        listValue = d[key]
        while (len(listValue) < count):
            listValue.append(0)
    return d


def createBinaryDictOfData(filename):
    d = {}
    with open(filename) as data:
        lineCount = 0
        for line in data:
            for word in line.split():
                if word not in d:
                    d[word] = list()
                    listValue = d[word]
                    for i in xrange(0, lineCount, 1):
                        listValue.append(0)
                    listValue.insert(lineCount, 1)
                else:
                    listValue = d[word]
                    try:
                        countLine = listValue[lineCount]
                        if (countLine >= 1):
                            continue
                        else:
                            countLine += 1;
                        listValue[lineCount] = countLine
                    except IndexError:
                        while (len(listValue) < lineCount):
                            listValue.append(0)
                        listValue.append(1)
                d[word] = listValue
            lineCount = lineCount + 1
    # print lineCount
    for key in d:
        listValue = d[key]
        while (len(listValue) < lineCount):
            listValue.append(0)
    return d
def getWordCountListFromFile(filename):
    d = createBinaryDictOfData(filename)
    #d = createDictOfData(filename)
    # print len(d.keys())
    train_key = list();
    for i in xrange(0, getSizeofData(filename), 1):
        temp_list = list();
        for key in d:
            listTrain = d[key]
            temp_list.append(listTrain[i])
        train_key.append(temp_list)
    return train_key


def occurrences(list1):
    no_of_examples = len(list1)
    prob = dict(Counter(list1))
    for key in prob.keys():
        prob[key] = prob[key] / float(no_of_examples)
    return prob

def occurrencesLikelihoodDict(list1):
    no_of_examples = len(list1)
    prob = dict(Counter(list1))
    if (len(prob) == 1):
        keys = prob.keys()
        if(keys[0]==0):
            prob[1]=no_of_examples-prob[keys[0]]
        elif(keys[0]==1):
            temp = prob[keys[0]]
            prob[0] = no_of_examples-temp
            prob[1] = temp
    for key in prob.keys():
        prob[key] = prob[key] / float(no_of_examples)
    return prob


def naive_bayes(training, outcome, new_sample):
    classes = np.unique(outcome)
    rows, cols = np.shape(training)
    #print "rows : " + str(rows) + " cols : " + str(cols)
    likelihoods = {}
    for cls in classes:
        likelihoods[cls] = defaultdict(list)

    class_probabilities = occurrences(outcome)
    # print "class_probabilities : " + class_probabilities

    for cls in classes:
        row_indices = np.where(outcome == cls)[0]
        subset = training[row_indices, :]
        r, c = np.shape(subset)
        for j in range(0, c):
            likelihoods[cls][j] += list(subset[:, j])

    for cls in classes:
        for j in range(0, cols):
            likelihoods[cls][j] = occurrencesLikelihoodDict(likelihoods[cls][j])

    #print likelihoods
    results = {}
    resultList = list()
    for item in new_sample:
        for cls in classes:
            class_probability = class_probabilities[cls]
            for i in range(0, len(item)):
                relative_values = likelihoods[cls][i]
                if(len(relative_values) != 0):
                    if item[i] in relative_values.keys():
                        class_probability *= relative_values[item[i]]
                    else:
                        class_probability *= 0.00000001
                        break
            results[cls] = class_probability
        if (results[0] > results[1]):
            resultList.append(0)
        else:
            resultList.append(1)
    return resultList


''' Start to calculate the Naive Bayes Classifier'''


def getLabelIndexList(filename):
    labelList = list()
    with open(filename) as data:
        for line in data:
            labelList.append(int(line))
    return labelList


def getLabelListFromParent():
    labelList = list()
    with open("farm-ads-label.txt") as test:
        for line in test:
            data = line.split()[1]
            labelList.append(int(data))
    return labelList


def getLabelList(filename):
    labelIndex = getLabelIndexList(filename)
    labelParentList = getLabelListFromParent()
    labelList = list()
    for i in xrange(0, len(labelIndex), 1):
        labelList.append(labelParentList[i])
    return labelList

#-------------------------- test case = 0.1 --------------------------


def naiveBayes(limit):
    with open("farm-ads.txt") as data:
        count = 0
        with open("test_data.txt", 'w') as test:
            with open("train_data.txt", 'w') as train:
                with open("test_label_index.txt", 'w') as test_label:
                    with open("train_label_index.txt", 'w') as train_label:
                        for line in data:
                            if random.random() < limit:
                                train.write(line)
                                train_label.write(str(count) + "\n")
                                count += 1
                            else:
                                test.write(line)
                                test_label.write(str(count) + "\n")
                                count += 1

    # print createDictOfData("testdara.txt")
    # print createBinaryDictOfData("testdara.txt")

    training = np.asarray(getWordCountListFromFile("train_data.txt"));
    matchcount= 0
    outcome = np.asarray(getLabelList("train_label_index.txt"))
    new_sample = np.asarray(getWordCountListFromFile("test_data.txt"))
    prediction = naive_bayes(training, outcome, new_sample)
    resultList = getLabelList("test_label_index.txt")
    # print prediction
    # print resultList


    for i in xrange(0, len(prediction), 1):
        if prediction[i] is resultList[i]:
            matchcount += 1
    print matchcount * 100 / getSizeofData("test_label_index.txt")

naiveBayes(0.1)
naiveBayes(0.3)
naiveBayes(0.5)
naiveBayes(0.7)
naiveBayes(0.8)
naiveBayes(0.9)

