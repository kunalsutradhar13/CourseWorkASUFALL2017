import csv
import numpy as numpyVar
import matplotlib.patches as mpatches
import matplotlib.pyplot as plt

varList1 = []
varList2 = []
varList3 = []
varList4 = []

with open('Result_LR_FromScratch.csv') as csvFileInput:
	listOfDict = csv.DictReader(csvFileInput)
	for dictionary in listOfDict:
		varList1.append(float(dictionary['Accuracy']))
		varList2.append(float(dictionary['TrainDataSize']))
valArray1 = numpyVar.asarray(varList2)
valArray2 = numpyVar.asarray(varList1)
print valArray1
print valArray2


with open('Result_NB_FromScratch.csv') as csvFileInput:
	listOfDict = csv.DictReader(csvFileInput)
	for dictionary in listOfDict:
		varList3.append(float(dictionary['Accuracy']))
		varList4.append(float(dictionary['TrainDataSize']))
valArray3 = numpyVar.asarray(varList4)
valArray4 = numpyVar.asarray(varList3)
print valArray3
print valArray4


plt.plot(valArray1, valArray2, color = "blue")
plt.plot(valArray3, valArray4, color = "red")
plt.show()



