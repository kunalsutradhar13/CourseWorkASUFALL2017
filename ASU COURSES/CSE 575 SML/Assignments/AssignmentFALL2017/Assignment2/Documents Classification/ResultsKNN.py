import csv
import numpy as numpyVar
import matplotlib.pyplot as plotLibVar
import matplotlib.pyplot as plt
import plotly as py
import plotly.graph_objs as go
from numpy import array
varList1 = []
varList2 = []
with open('ResultsForKNN.csv') as csvFileInput:
	listOfDict = csv.DictReader(csvFileInput)
	for dictionary in listOfDict:
		varList1.append(float(dictionary['Accuracy']))
		varList2.append(float(dictionary['K']))
valArray1 = numpyVar.asarray(varList1)
valArray2 = numpyVar.asarray(varList2)
print valArray1
print valArray2
plt.plot(valArray1, valArray2)
plt.show()


