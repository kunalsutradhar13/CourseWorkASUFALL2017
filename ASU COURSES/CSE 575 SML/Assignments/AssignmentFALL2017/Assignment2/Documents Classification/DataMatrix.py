def createDictOfData(filename):
    d = {}
    with open(filename) as data:
        count = 0
        for line in data:
            for word in line.split():
                if word not in d:
                    d[word] = list()
                    listValue = d[word]
                    for i in xrange(0, count, 1):
                        listValue.append(0)
                    listValue.insert(count, 1)
                else:
                    listValue = d[word]
                    try:
                        countLine = listValue[count]
                        countLine += 1;
                        listValue[count] = countLine
                    except IndexError:
                        while (len(listValue) < count):
                            listValue.append(0)
                        listValue.append(1)
                d[word] = listValue
            count = count + 1
    # print count
    for key in d:
        listValue = d[key]
        while (len(listValue) < count):
            listValue.append(0)
    return d
file = open("DataMatrixFile.txt","w")
file.write(str(createDictOfData("farm-ads.txt")))
file.close()