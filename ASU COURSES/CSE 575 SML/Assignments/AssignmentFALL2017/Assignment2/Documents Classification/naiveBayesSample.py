from collections import Counter, defaultdict
from sklearn.naive_bayes import GaussianNB
import numpy as np

def occurrences(list1):
    no_of_examples = len(list1)
    prob = dict(Counter(list1))
    for key in prob.keys():
        prob[key] = prob[key] / float(no_of_examples)
    return prob

def occurrencesLikelihoodDict(list1):
    no_of_examples = len(list1)
    prob = dict(Counter(list1))
    if (len(prob) == 1):
        keys = prob.keys()
        if(keys[0]==0):
            prob[1]=no_of_examples-prob[keys[0]]
        elif(keys[0]==1):
            temp = prob[keys[0]]
            prob[0] = no_of_examples-temp
            prob[1] = temp
    for key in prob.keys():
        prob[key] = prob[key] / float(no_of_examples)
    return prob


def naive_bayes(training, outcome, new_sample):
    classes     = np.unique(outcome)
    rows, cols  = np.shape(training)
    likelihoods = {}

    #print classes 
    #print rows 
    #print cols 
    for cls in classes:
        likelihoods[cls] = defaultdict(list)
 
    class_probabilities = occurrences(outcome)
    
    #print class_probabilities

    for cls in classes:
        row_indices = np.where(outcome == cls)[0]
        subset      = training[row_indices, :]
        r, c        = np.shape(subset)
        for j in range(0,c):
            likelihoods[cls][j] += list(subset[:,j])
    
    #print likelihoods 

    for cls in classes:
        for j in range(0,cols):
             likelihoods[cls][j] = occurrencesLikelihoodDict(likelihoods[cls][j])
    #print likelihoods 

    results = {}
    resultList = list()
    for item in new_sample:
        for cls in classes:
             class_probability = class_probabilities[cls]
             for i in range(0,len(item)):
                     relative_values = likelihoods[cls][i]
                     if item[i] in relative_values.keys():
                         class_probability *= relative_values[item[i]]
                     else:
                         class_probability *= 0
                         break
             results[cls] = class_probability
        print results
        if (results[0] > results[1]):
            resultList.append(0)
        else:
            resultList.append(1)



    print resultList
 
if __name__ == "__main__":

    # gnb = GaussianNB()
    # y_pred = gnb.fit(np.asarray(((1,0,1,1),(1,1,0,0),(1,0,1,1),(0,1,1,1),(0,0,0,0),(0,1,1,1),(0,1,1,0),(1,1,1,1))), np.asarray([0,1,1,1,0,1,0,1])).predict(np.asarray([[1,0,1,0]]))
    # print y_pred
    
    #training   = np.asarray(((0,0,0,0),(1,1,1,1),(1,1,1,1),(1,1,1,1),(0,0,0,0),(1,1,1,1),(0,0,0,0),(1,1,1,1)))
    training   = np.asarray([[1,0,1,1],[1,1,0,0],[1,0,2,1],[0,1,1,1],[0,0,0,0],[0,1,2,1],[0,1,2,0],[1,1,1,1]]);
    outcome    = np.asarray([0,1,1,1,0,1,0,1])
    new_sample = np.asarray([[1,0,0,1,0], [1,0,1,0]])
    naive_bayes(training, outcome, new_sample)