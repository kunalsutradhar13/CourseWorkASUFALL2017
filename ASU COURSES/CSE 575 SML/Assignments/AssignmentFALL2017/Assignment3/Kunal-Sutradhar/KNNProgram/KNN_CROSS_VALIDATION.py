import pandas as pd
import sklearn.metrics.pairwise
import scipy.sparse
import time


def KNN(trainSparse, testSparse, trainLabelDf, testLabelDf, k):
    start_time = time.time()
    solutionDict = {}
    finalClassificationCount = []
    resultDict = {}
    for i in range(0, testSparse.shape[0]):
        Kdict = {}

        # dist_64_sklearn = sklearn.metrics.pairwise.pairwise_distances(trainSparse, testSparse)
        dist_64_sklearnIndi = sklearn.metrics.pairwise.pairwise_distances(trainSparse, testSparse[i])
        # print dist_64_sklearn.shape
        # print dist_64_sklearnIndi.shape
        Smallest100Distances = dist_64_sklearnIndi.ravel()
        # print Smallest100Distances.shape
        for k_value in k:
            trainLabelList = []
            listResult = Smallest100Distances.argsort()[:k_value]
            # print Smallest100Distances.argsort()[:k]
            Kdict[i] = listResult.tolist()

            # print len(listResult)
            # print "---"
            for j in Kdict[i]:
                trainLabelList.extend(trainLabelDf.values.tolist()[j])
            # print Kdict
            # print k_value
            # print trainLabelList.count(0.0)
            # print trainLabelList.count(1.0)

            # print trainLabelList

            if (trainLabelList.count(0.0) >= trainLabelList.count(1.0)):
                # print "classifying test case : " + str(i) + " as " + " 0 "
                # finalClassificationCount.append(0)
                if k_value not in solutionDict:
                    listOfKClassifications = []
                    listOfKClassifications.append(0)
                    solutionDict[k_value] = listOfKClassifications
                else:
                    listOfKClassifications = solutionDict[k_value]
                    listOfKClassifications.append(0)
                    solutionDict[k_value] = listOfKClassifications

            else:
                # print "classifying test case : " + str(i) + " as " + " 1 "
                # finalClassificationCount.append(1)
                if k_value not in solutionDict:
                    listOfKClassifications = []
                    listOfKClassifications.append(1)
                    solutionDict[k_value] = listOfKClassifications
                else:
                    listOfKClassifications = solutionDict[k_value]
                    listOfKClassifications.append(1)
                    solutionDict[k_value] = listOfKClassifications

                    # print "---"

    # print solutionDict

    testList = []
    for j in range(0, testSparse.shape[0]):
        testList.extend(testLabelDf.values.tolist()[j])

    for k_value in k:
        finalClassificationCount = solutionDict[k_value]
        count = 0
        for i in range(0, testSparse.shape[0]):
            if (testList[i] == finalClassificationCount[i]):
                count += 1
        print "accuracy for k = " + str(k_value) + " : " + str(count * 100.0 / (testSparse.shape[0]))
        resultDict[k_value] = count * 100.0 / (testSparse.shape[0])
    end_time = time.time()
    print str(end_time - start_time)
    return resultDict


def CrossValidation(knn_trainData_File, knn_testData_File,fold):
    # print start_time

    trainDataDf = pd.read_csv(knn_trainData_File, header=None)
    # trainLabelDf = pd.read_csv('knn_train_label.csv', header=None)
    # totalDataFrame = pd.concat([trainDataDf, trainLabelDf], ignore_index=True)
    trainSparse = scipy.sparse.csr_matrix(trainDataDf.values)

    foldValue = trainSparse.shape[0] / fold
    print foldValue

    answerDict = {}

    trainDataStack = pd.read_csv(knn_trainData_File, header=None)
    trainLabelStack = pd.read_csv(knn_testData_File, header=None)
    trainDataAndLabelStack = pd.concat([trainDataStack, trainLabelStack], ignore_index=True, axis=1)
    for i in range(0, fold):

        #samplingCode:

        trainDataDf = pd.read_csv(knn_trainData_File, header=None)
        trainLabelDf = pd.read_csv(knn_testData_File, header=None)
        trainDataAndLabel = pd.concat([trainDataDf, trainLabelDf], ignore_index=True, axis=1)

        testStack = trainDataAndLabelStack.sample(n=foldValue)
        trainDataAndLabelStack = trainDataAndLabelStack[~trainDataAndLabelStack.isin(testStack)].dropna()
        trainDataAndLabel = trainDataAndLabel[~trainDataAndLabel.isin(testStack)].dropna()

        actualTrainLabelData = trainDataAndLabel.iloc[:, 166:]
        actualTrainData = trainDataAndLabel.iloc[:, :166]

        # FOR PCA ONLY
        # actualTrainLabelData = trainDataAndLabel.iloc[:, 50:]
        # actualTrainData = trainDataAndLabel.iloc[:, :50]


        actualTrainSparse = scipy.sparse.csr_matrix(actualTrainData.values)

        print "-- train Shapes --"
        print actualTrainData.shape
        print actualTrainLabelData.shape
        print "-- over train shapes --"

        actualTestData = testStack.iloc[:, :166]
        actualTestSparse = scipy.sparse.csr_matrix(actualTestData.values)
        actualTestLabelData = testStack.iloc[:, 166:]

        #  for PCA ONLY
        # actualTestData = testStack.iloc[:, :50]
        # actualTestSparse = scipy.sparse.csr_matrix(actualTestData.values)
        # actualTestLabelData = testStack.iloc[:, 50:]

        print "-- test Shapes --"
        print actualTestData.shape
        print actualTestLabelData.shape
        print "-- over test shapes --"


        k = [1, 3, 5, 7, 9, 11, 13, 15, 17]

        solutionDict = KNN(actualTrainSparse, actualTestSparse, actualTrainLabelData, actualTestLabelData, k)
        answerDict[i] = solutionDict

    # print answerDict
    accuracyDict = {}
    for k in answerDict:
        # print answerDict[k]
        for j in answerDict[k]:
            listOfAccuracies = answerDict[k][j]
            if j not in accuracyDict:
                accuracyDict[j] = listOfAccuracies
            else:
                accuracyDict[j] = accuracyDict[j] + listOfAccuracies
    # print accuracyDict

    for t in accuracyDict:
        accuracyDict[t] = accuracyDict[t] / float(fold)

    print accuracyDict


def standardKNN():
    start_time = time.time()
    # print start_time

    testDataDf = pd.read_csv('knn_test_data.csv', header=None)
    # testDataDf = pd.read_csv('KNN_PCA_test_Data.csv', header=None)
    testSparse = scipy.sparse.csr_matrix(testDataDf.values)

    trainDataDf = pd.read_csv('knn_train_data.csv', header=None)
    # trainDataDf = pd.read_csv('KNN_PCA_Data.csv', header=None)
    trainSparse = scipy.sparse.csr_matrix(trainDataDf.values)

    trainLabelDf = pd.read_csv('knn_train_label.csv', header=None)
    testLabelDf = pd.read_csv('knn_test_label.csv', header=None)

    # k = [1, 3, 5, 7, 9, 11, 13, 15, 17]
    k=[5]
    KNN(trainSparse, testSparse, trainLabelDf, testLabelDf, k)

    end_time = time.time()
    print "total time taken  : " + str(end_time - start_time) + " seconds"


if __name__ == '__main__':
    # standardKNN()
    CrossValidation('knn_train_data.csv','knn_train_label.csv',5)   #without pca
    #CrossValidation('KNN_PCA_Data.csv','knn_train_label.csv',5)  # with pca