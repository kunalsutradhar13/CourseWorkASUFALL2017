# Generating the CSV from kmeans_data.mat file given:
import csv
import scipy.io
import numpy as np
import pandas as pd

# data = scipy.io.loadmat("kmeans_data.mat")
# with open('kmeans_data.csv', 'w') as csvfile:
#     # fieldnames = []
#     # for i in range(0, 21):
#     #     fieldnames.append("feature" + str(i))
#     # print fieldnames
#     # writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
#     # writer.writeheader()
#     for i in data:
#         if '__' not in i and 'readme' not in i:
#             np.savetxt(("kmeans_data.csv"), data[i], delimiter=',')

data = scipy.io.loadmat("knn_data.mat")
# with open('knn_data.csv', 'w') as csvfile:
    # fieldnames = []
    # for i in range(0, 21):
    #     fieldnames.append("feature" + str(i))
    # print fieldnames
    # writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    # writer.writeheader()
for i in data:
    print i
    if 'test_data' in i and '__header__' not in i and '__version__' not in i and 'readme' not in i:
        np.savetxt(("knn_test_data.csv"), data[i], delimiter=',')
    if 'test_label' in i and '__header__' not in i and '__version__' not in i and 'readme' not in i:
        np.savetxt(("knn_test_label.csv"), data[i], delimiter=',')
    if 'train_label' in i and '__header__' not in i and '__version__' not in i and 'readme' not in i:
        np.savetxt(("knn_train_label.csv"), data[i], delimiter=',')
    if 'train_data' in i and '__header__' not in i and '__version__' not in i and 'readme' not in i:
        np.savetxt(("knn_train_data.csv"), data[i], delimiter=',')

# df1 = pd.read_csv('knn_test_data.csv')
# df2 = pd.read_csv('knn_test_label.csv')
# print df1.join(df2)
# df1.to_csv('knn_test_forAssign3.csv',header = False, encoding='utf-8', index=False)