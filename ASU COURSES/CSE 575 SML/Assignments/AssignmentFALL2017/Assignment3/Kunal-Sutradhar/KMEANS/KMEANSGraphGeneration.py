import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('KmeansGraph.csv', header=None)
x = df[0]
y = df[1]
df2 = pd.read_csv('KmeansppGraph.csv', header=None)
x2= df2[0]
y2= df2[1]
line_down, = plt.plot(x2,y2, label='Line 2')
line_up, = plt.plot(x,y, label='Line 1')
plt.legend([line_up, line_down], ['KMEANS', 'KMEANS++'])
plt.show()