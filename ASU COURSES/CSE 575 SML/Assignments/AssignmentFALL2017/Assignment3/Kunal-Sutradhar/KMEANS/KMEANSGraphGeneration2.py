import pandas as pd
import matplotlib.pyplot as plt
df2 = pd.read_csv('KmeansGraph.csv', header=None)
x2= df2[0]
y2= df2[1]
line_up, = plt.plot(x2,y2, label='Line 2')
plt.legend([line_up], ['KMEANS'])
plt.show()