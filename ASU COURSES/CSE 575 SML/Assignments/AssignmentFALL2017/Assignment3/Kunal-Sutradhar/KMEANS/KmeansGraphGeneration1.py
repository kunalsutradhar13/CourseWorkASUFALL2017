import pandas as pd
import matplotlib.pyplot as plt
df2 = pd.read_csv('KmeansppGraph.csv', header=None)
x2= df2[0]
y2= df2[1]
line_down, = plt.plot(x2,y2, label='Line 2')
plt.legend([line_down], ['KMEANS++'])
plt.show()