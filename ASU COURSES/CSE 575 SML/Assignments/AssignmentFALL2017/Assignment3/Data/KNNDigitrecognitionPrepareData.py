
from mnist import MNIST
import csv

f = open('output.txt','w')
mndata = MNIST('train')
(images, labels) = mndata.load_training()
mndata1 = MNIST('test')
(images1, labels1) = mndata1.load_testing()



# print type(images)
# print images[0]
# print images[-1]
# print labels[0]
# print labels[-1]
# print type(images1)
i=0
for record in images:
    record.append(labels[i])
    i+=1

i=0
for record in images1:
    record.append(labels1[i])
    i+=1

with open("KNNTrainData.csv", "wb") as f:
    writer = csv.writer(f)
    writer.writerows(images)
f.close()


with open("KNNTestData.csv", "wb") as f:
    writer = csv.writer(f)
    writer.writerows(images1)
f.close()

