import matplotlib.pyplot as plt
from matplotlib import style
import csv
import scipy
import scipy.sparse.linalg as scp
from scipy.sparse import *

style.use('ggplot')
import numpy as np

from numpy.core import multiarray as mu
from numpy.core import umath as um
from numpy.core import numerictypes as nt

# save those O(100) nanoseconds!
umr_maximum = um.maximum.reduce
umr_minimum = um.minimum.reduce
umr_sum = um.add.reduce
umr_prod = um.multiply.reduce
umr_any = um.logical_or.reduce
umr_all = um.logical_and.reduce

X = np.array([[1, 2],
              [1.5, 1.8],
              [5, 8],
              [8, 8],
              [1, 0.6],
              [9, 11],
              [1, 3],
              [8, 9],
              [0, 3],
              [5, 4],
              [6, 4], ])


def extract_nonzero(fname):
    """
    extracts nonzero entries from a csv file
    input: fname (str) -- path to csv file
    output: generator<(int, int, float)> -- generator
            producing 3-tuple containing (row-index, column-index, data)
    """
    for (rindex, row) in enumerate(csv.reader(open(fname))):
        for (cindex, data) in enumerate(row):
            if data != "0":
                yield (rindex, cindex, float(data))


def get_dimensions(fname):
    """
    determines the dimension of a csv file
    input: fname (str) -- path to csv file
    output: (nrows, ncols) -- tuple containing row x col data
    """
    rowgen = (row for row in csv.reader(open(fname)))
    # compute col size
    colsize = len(rowgen.next())
    # compute row size
    rowsize = 1 + sum(1 for row in rowgen)
    return (rowsize, colsize)


# obtain dimensions of data
(rdim, cdim) = get_dimensions("kmeans_data.csv")

# allocate a lil_matrix of size (rdim by cdim)
# note: lil_matrix is used since we be modifying
#       the matrix a lot.
S = lil_matrix((rdim, cdim))

# add data to S
for (i, j, d) in extract_nonzero("kmeans_data.csv"):
    S[i, j] = d

##plt.scatter(X[:,0], X[:,1], s=150)
##plt.show()

colors = 10 * ["g", "r", "c", "b", "k"]


class K_Means:
    def __init__(self, k=2, tol=0.001, max_iter=300):
        self.k = k
        self.tol = tol
        self.max_iter = max_iter

    def fit(self, data):

        self.centroids = {}

        for i in range(self.k):
            self.centroids[i] = data[i]

        for i in range(self.max_iter):
            self.classifications = {}

            for i in range(self.k):
                self.classifications[i] = []

            for featureset in data:
                distances = [scp.norm(featureset - self.centroids[centroid]) for centroid in self.centroids]
                classification = distances.index(min(distances))
                self.classifications[classification].append(featureset)

            prev_centroids = dict(self.centroids)

            # print type(self.classifications)
            for classification in self.classifications:
                # print type(self.classifications[classification])
                self.centroids[classification] = meanValues(self.classifications[classification], axis=0, dtype=object)

            optimized = True

            for c in self.centroids:
                original_centroid = prev_centroids[c]
                current_centroid = self.centroids[c]
                if np.sum((current_centroid - original_centroid) / original_centroid * 100.0) > self.tol:
                    # print(np.sum((current_centroid - original_centroid) / original_centroid * 100.0))
                    optimized = False

            if optimized:
                break

    def predict(self, data):
        distances = [np.linalg.norm(data - self.centroids[centroid]) for centroid in self.centroids]
        classification = distances.index(min(distances))
        return classification


def meanValues(a, axis=None, dtype=None, out=None, keepdims=False):
    arr = np.asanyarray(a)

    is_float16_result = False
    rcount = count_reduce_items(arr, axis)
    # Make this warning show up first

    # Cast bool, unsigned int, and int to float64 by default
    if dtype is None:
        if issubclass(arr.dtype.type, (nt.integer, nt.bool_)):
            dtype = mu.dtype('f8')
        elif issubclass(arr.dtype.type, nt.float16):
            dtype = mu.dtype('f4')
            is_float16_result = True

    ret = umr_sum(arr, axis, dtype, out, keepdims)
    if isinstance(ret, mu.ndarray):
        ret = um.true_divide(
            ret, rcount, out=ret, casting='unsafe', subok=False)
        if is_float16_result and out is None:
            ret = arr.dtype.type(ret)
    elif hasattr(ret, 'dtype'):
        if is_float16_result:
            ret = arr.dtype.type(ret / rcount)
        else:
            ret = (ret / rcount)
    else:
        ret = ret / rcount

    return ret


def count_reduce_items(arr, axis):
    if axis is None:
        axis = tuple(range(arr.ndim))
    if not isinstance(axis, tuple):
        axis = (axis,)
    items = 1
    for ax in axis:
        items *= arr.shape[ax]
    return items


for i in range(2, 11):
    print "-----------" + "k = " + str(i) + "--------------"
    clf = K_Means(k=i)
    clf.fit(S)
    # print clf.centroids
    for j in range(0, i):
        print str(clf.centroids[j])
        print "-------------------------"

# for centroid in clf.centroids:
#     #print str(clf.centroids[centroid][0]) +" , "+ str(clf.centroids[centroid][1])
#     plt.scatter(clf.centroids[centroid][0], clf.centroids[centroid][1],
#                 marker="o", color="k", s=150, linewidths=5)
#
# for classification in clf.classifications:
#     color = colors[classification]
#     for featureset in clf.classifications[classification]:
#         #print clf.classifications[classification]
#         plt.scatter(featureset[0], featureset[1], marker="x", color=color, s=150, linewidths=5)


# plt.show()
