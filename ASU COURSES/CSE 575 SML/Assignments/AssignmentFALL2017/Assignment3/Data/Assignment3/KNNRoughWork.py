import scipy.sparse
from sklearn.metrics.pairwise import euclidean_distances
import sklearn
from scipy.sparse import triu
import pandas as pd
import numpy as np

data = [4,1,2,2,1,1,4,3,2]
col = [0,1,1,2,2,3,4,4,4]
row = [2,0,4,0,3,5,0,2,3]
M = scipy.sparse.csr_matrix((data,(row,col)))



print M.toarray()
print triu(M,k=0).toarray()
print "-------"
print euclidean_distances(M, M)
print "-------"
print M.shape
distances = sklearn.metrics.pairwise.pairwise_distances(M,M[0])
print distances.shape
print distances
b = distances.ravel()
print b
print b.shape
print b.argsort()[::-1][:3]  #largest 3 values in the nparray
print b.argsort()[:3]  #smallest 3 values in the nparray


print "--j---"

testDataDf = pd.read_csv('knn_test_data.csv', header=None)
print type(testDataDf)
#var =  pd.merge(testDataDf.iloc[2:4], testDataDf.iloc[0:2])
e1 = testDataDf.iloc[2:4]
e2 = testDataDf.iloc[0:2]
e3 = pd.DataFrame(np.random.randint(0,100,size=(4, 1)))
print e3.shape
print "print e1"
print 
print e1
print e2
e = pd.concat([e1, e2], ignore_index=True)
print
print "printing e.shape : " + str(e.shape)
eprime = pd.concat([e, e3], ignore_index=True, axis=1)
print "print ing the datat  : "
print
print  eprime.shape
#e = pd.merge(e1, e2, how='outer')
print eprime

new_df = eprime.iloc[:, 166:]
remainingDf = eprime.iloc[:, :166]

print new_df.shape
print eprime.shape
print remainingDf.shape
# print remainingDf
print "--j---"

# Randomly sample 70% of your dataframe

print
for i in range(0,2):
    actualDf = pd.read_csv('knn_test_data.csv', header=None)

    df_7 = testDataDf.sample(n=319)

    print "df_7 shape "
    print df_7.shape

    testDataDf = testDataDf[~testDataDf.isin(df_7)].dropna()
    print "testdaf shape "
    print testDataDf.shape

    print

    actualDf = actualDf[~actualDf.isin(df_7)].dropna()

    print "actual shape "
    print actualDf.shape

    print

# # Randomly sample 7 elements from your dataframe
# df_1 = testDataDf.sample(n=7)
#
# print df_1


 # print str(i * foldValue) + " " + str(i * foldValue + foldValue) + " " + str(trainSparse.shape[0])
        # actualTestData = trainDataDf.iloc[i * foldValue:i * foldValue + foldValue]
        # actualTestSparse = scipy.sparse.csr_matrix(actualTestData.values)
        #
        # partialTrainData1 = trainDataDf.iloc[0:i * foldValue]
        # partialTrainData2 = trainDataDf.iloc[i * foldValue + foldValue:trainSparse.shape[0]]
        # actualTrainData = pd.concat([partialTrainData1, partialTrainData2], ignore_index=True)
        # actualTrainSparse = scipy.sparse.csr_matrix(actualTrainData.values)
        #
        # trainLabelDf = pd.read_csv('knn_train_label.csv', header=None)
        # partialTrainLabel1 = trainLabelDf.iloc[0:i * foldValue]
        # partialTrainLabel2 = trainLabelDf.iloc[i * foldValue + foldValue:trainSparse.shape[0]]
        # actualTrainLabelData = pd.concat([partialTrainLabel1, partialTrainLabel2], ignore_index=True)
        #
        # actualTestLabelData = trainLabelDf.iloc[i * foldValue:i * foldValue + foldValue]