import pandas as pd
import matplotlib.pyplot as plt


KNN_k1 = [1, 3, 5, 7, 11, 13, 15, 17, 19]
KNN_WithoutPCA_ACC = [94.38000000000001, 95.6, 96.08, 95.86, 95.68, 95.62, 95.32000000000001, 95.33999999999999, 95.24]
df2= pd.DataFrame(list(zip(KNN_k1, KNN_WithoutPCA_ACC)))

x2 = df2[0]
y2 = df2[1]
line_down, = plt.plot(x2, y2, label='Cross Validation KNN Without PCA')
plt.legend([line_down], ['Cross Validation KNN Without PCA'])
plt.show()