## Author: Nikita Shanker

import pandas as pd

import numpy as np
import sklearn.metrics.pairwise
import scipy.sparse
import pandas as pd
from scipy import sparse
from collections import defaultdict
from sklearn.metrics import pairwise_distances
from itertools import chain


def func_KNN(X_train, X_test, label_train, label_test, kList):
    temp_dict = {}
    result = {}

    for i in range(0, X_test.shape[0]):
        curr_k_dict = {}

        test_distances = sklearn.metrics.pairwise.pairwise_distances(X_train, X_test[i]).ravel()

        for item in kList:
            curr_labels_train = []
            curr_k_dict[i] = test_distances.argsort()[:item].tolist()
            for j in curr_k_dict[i]:
                curr_labels_train.extend(label_train.values.tolist()[j])

            if (curr_labels_train.count(0.0) >= curr_labels_train.count(1.0)):

                if item not in temp_dict:
                    list_k_cur = []
                    list_k_cur.append(0)
                    temp_dict[item] = list_k_cur
                else:
                    list_k_cur = temp_dict[item]
                    list_k_cur.append(0)
                    temp_dict[item] = list_k_cur

            else:

                if item not in temp_dict:
                    list_k_cur = []
                    list_k_cur.append(1)
                    temp_dict[item] = list_k_cur
                else:
                    list_k_cur = temp_dict[item]
                    list_k_cur.append(1)
                    temp_dict[item] = list_k_cur

    testList = []
    for j in range(0, X_test.shape[0]):
        testList.extend(label_test.values.tolist()[j])

    assignedList = []
    for item in kList:
        assignedList = temp_dict[item]
        count = 0
        for i in range(0, X_test.shape[0]):
            if (testList[i] == assignedList[i]):
                count += 1
        print "Accuracy for k = " + str(item) + " : " + str(count * 100.0 / (X_test.shape[0]))
        result[item] = count * 100.0 / (X_test.shape[0])

    return result



def func_KNN_CrossValidation(train_x_dataset, train_label_dataset,fold):

    X_train = pd.read_csv(train_x_dataset, header=None)

    X_train_sparse = scipy.sparse.csr_matrix(X_train.values)

    fld_val = X_train_sparse.shape[0] / fold

    resultSet = {}

    csv_train_x = pd.read_csv(train_x_dataset, header=None)
    csv_train_label = pd.read_csv(train_label_dataset, header=None)
    csv_train_all = pd.concat([csv_train_x, csv_train_label], ignore_index=True, axis=1)
    for i in range(0, fold):

        #sampling on the dataset being performed here:

        X_train = pd.read_csv(train_x_dataset, header=None)
        label_train = pd.read_csv(train_label_dataset, header=None)
        current_train_all = pd.concat([X_train, label_train], ignore_index=True, axis=1)

        loocv_test_data = csv_train_all.sample(n=fld_val)
        csv_train_all = csv_train_all[~csv_train_all.isin(loocv_test_data)].dropna()
        current_train_all = current_train_all[~current_train_all.isin(loocv_test_data)].dropna()

        # FOR PCA validations

        act_tr_lbls = current_train_all.iloc[:, 166:]
        act_train_x = current_train_all.iloc[:, :166]
        act_train_x_sparse = scipy.sparse.csr_matrix(act_train_x.values)

        act_tst_x = loocv_test_data.iloc[:, :166]
        act_tst_lbls = loocv_test_data.iloc[:, 166:]
        act_tst_sparse = scipy.sparse.csr_matrix(act_tst_x.values)


        # train data shapes
        print "-- Train Data Shapes --"
        print act_train_x.shape
        print act_tr_lbls.shape
        print "-- Over Train Data shapes --"

        # test data shapes

        print "-- Test Data Shapes --"
        print act_tst_x.shape
        print act_tst_lbls.shape
        print "-- Over Test shapes --"


        k = [1, 3, 5, 7, 9, 11, 13, 15, 17]

        KNN_values = func_KNN(act_train_x_sparse, act_tst_sparse, act_tr_lbls, act_tst_lbls, k)
        resultSet[i] = KNN_values

    cross_validn_accuracies = {}
    for k in resultSet:
        for j in resultSet[k]:
            temp = resultSet[k][j]
            if j not in cross_validn_accuracies:
                cross_validn_accuracies[j] = temp
            else:
                cross_validn_accuracies[j] = cross_validn_accuracies[j] + temp

    for t in cross_validn_accuracies:
        cross_validn_accuracies[t] = cross_validn_accuracies[t] / float(fold)

    print cross_validn_accuracies

func_KNN_CrossValidation('knn_train_data.csv','knn_train_label.csv',5)
