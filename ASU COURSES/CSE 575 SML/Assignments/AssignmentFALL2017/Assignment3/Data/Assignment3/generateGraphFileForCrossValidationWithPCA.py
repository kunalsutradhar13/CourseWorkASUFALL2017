import pandas as pd
import matplotlib.pyplot as plt


KNN_k = [1, 3, 5, 7, 11, 13, 15, 17, 19]
KNN_Accuracy = [95.03999999999999, 96.1, 96.2, 96.02000000000001, 95.8, 95.94000000000001, 95.64, 95.62,
                95.46000000000001]
df1 = pd.DataFrame(list(zip(KNN_k, KNN_Accuracy)))

x = df1[0]
y = df1[1]

line_up, = plt.plot(x,y, label='Cross Validation KNN With PCA')
plt.legend([line_up], ['Cross Validation KNN With PCA'])
plt.show()