import pandas as pd
import matplotlib.pyplot as plt

KNN_k = [1, 3, 5, 7, 11, 13, 15, 17, 19]
KNN_Accuracy = [95.03999999999999, 96.1, 96.2, 96.02000000000001, 95.8, 95.94000000000001, 95.64, 95.62,
                95.46000000000001]

KNN_k1 = [1, 3, 5, 7, 11, 13, 15, 17, 19]
KNN_WithoutPCA_ACC = [94.38000000000001, 95.6, 96.08, 95.86, 95.68, 95.62, 95.32000000000001, 95.33999999999999, 95.24]
df1 = pd.DataFrame(list(zip(KNN_k, KNN_Accuracy)))
df2= pd.DataFrame(list(zip(KNN_k1, KNN_WithoutPCA_ACC)))
x = df1[0]
y = df1[1]
x2 = df2[0]
y2 = df2[1]
line_up, = plt.plot(x,y, label='Cross Validation KNN With PCA')
line_down, = plt.plot(x2, y2, label='Cross Validation KNN Without PCA')
plt.legend([line_up, line_down], ['Cross Validation KNN With PCA', 'Cross Validation KNN Without PCA'])
plt.show()
