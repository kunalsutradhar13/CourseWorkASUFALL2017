import scipy.sparse
import scipy.stats
from sklearn.metrics.pairwise import euclidean_distances
import sklearn
from scipy.sparse import triu
import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
import pandas as pd
import numpy as np



trainDataDf = pd.read_csv('knn_train_data.csv', header=None)
# trainDataDf = pd.read_csv('knn_test_data.csv', header=None)
trainLabelDf = pd.read_csv('knn_train_label.csv', header=None)
# trainLabelDf = pd.read_csv('knn_test_label.csv', header=None)


X = trainDataDf.values
y = trainLabelDf.values
print type(X)
print type(y)

X_std = StandardScaler().fit_transform(X)
# print type(X_std)
print X_std.shape

mean_vec = np.mean(X_std, axis=0)
cov_mat = (X_std - mean_vec).T.dot((X_std - mean_vec)) / (X_std.shape[0]-1)
print('Covariance matrix \n%s' %cov_mat)
cov_mat = np.cov(X_std.T)

eig_vals, eig_vecs = np.linalg.eig(cov_mat)

print('Eigenvectors \n%s' %eig_vecs)
print('\nEigenvalues \n%s' %eig_vals)

u,s,v = np.linalg.svd(X_std.T)
print ('\nAfter SVD : u \n%s'%u)


for ev in eig_vecs:
    np.testing.assert_array_almost_equal(1.0, np.linalg.norm(ev))
print('Everything ok!')



# Make a list of (eigenvalue, eigenvector) tuples
eig_pairs = [(np.abs(eig_vals[i]), eig_vecs[:,i]) for i in range(len(eig_vals))]

# Sort the (eigenvalue, eigenvector) tuples from high to low
eig_pairs.sort(key=lambda x: x[0], reverse=True)

# Visually confirm that the list is correctly sorted by decreasing eigenvalues
print('Eigenvalues in descending order:')
for i in eig_pairs:
    print(i[0])

# print
# print type(eig_pairs)
# print
# print eig_pairs[0][1]
# print
# print type(eig_pairs[0][1])
# print
# print eig_pairs[0]
# print
# print eig_pairs
# print
tot = sum(eig_vals)
var_exp = [(i / tot)*100 for i in sorted(eig_vals, reverse=True)]
cum_var_exp = np.cumsum(var_exp)

# qer = (eig_pairs[0][1].reshape(4,1),eig_pairs[1][1].reshape(4,1),eig_pairs[2][1].reshape(4,1))


qer=()
for i in range(0,50):
    qer = qer+ (eig_pairs[i][1].reshape(166,1),)
# qer = (eig_pairs[0][1].reshape(166,1),eig_pairs[1][1].reshape(166,1),eig_pairs[2][1].reshape(166,1))



print type(qer)
matrix_w = np.hstack(qer)

print matrix_w.shape
print

print X_std.shape
print
print('Matrix W:\n', matrix_w)
Y = X_std.dot(matrix_w)
print type(Y)
print
print Y.shape
print


dfResult = pd.DataFrame(Y)
print dfResult.shape

dfResult.to_csv("KNN_PCA_train_Data.csv", sep=',',  header=False, index=False,  encoding='utf-8')
# dfResult.to_csv("KNN_PCA_test_Data.csv", sep=',',  header=False, index=False,  encoding='utf-8')
