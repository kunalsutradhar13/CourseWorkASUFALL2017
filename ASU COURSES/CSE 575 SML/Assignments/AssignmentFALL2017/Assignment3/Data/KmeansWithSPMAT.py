import csv
from scipy.sparse import *
from sklearn.cluster import KMeans

def extract_nonzero(fname):
  """
  extracts nonzero entries from a csv file
  input: fname (str) -- path to csv file
  output: generator<(int, int, float)> -- generator
          producing 3-tuple containing (row-index, column-index, data)
  """
  for (rindex,row) in enumerate(csv.reader(open(fname))):
    for (cindex, data) in enumerate(row):
      if data!="0":
        yield (rindex, cindex, float(data))

def get_dimensions(fname):
  """
  determines the dimension of a csv file
  input: fname (str) -- path to csv file
  output: (nrows, ncols) -- tuple containing row x col data
  """
  rowgen = (row for row in csv.reader(open(fname)))
  # compute col size
  colsize = len(rowgen.next())
  # compute row size
  rowsize = 1 + sum(1 for row in rowgen)
  return (rowsize, colsize)

# obtain dimensions of data
(rdim, cdim) = get_dimensions("kmeans_data.csv")

# allocate a lil_matrix of size (rdim by cdim)
# note: lil_matrix is used since we be modifying
#       the matrix a lot.
S = lil_matrix((rdim, cdim))

# add data to S
for (i,j,d) in extract_nonzero("kmeans_data.csv"):
  S[i,j] = d

# perform clustering
labeler = KMeans(3)
# convert lil to csr format
# note: Kmeans currently only works with CSR type sparse matrix
labeler.fit(S.tocsr())
C = labeler.cluster_centers_
print C

# print cluster assignments for each row
# for (row, label) in enumerate(labeler.labels_):
#   print "row %d has label %d"%(row, label)