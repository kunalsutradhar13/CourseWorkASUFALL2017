import numpy as np
import random
from numpy import genfromtxt
import pandas as pd


class KMeansClustering():
    def __init__(self, K):
        self.K = K
        self.X = self.setDataPoints()
        self.N = len(self.X)
        self.mu = None
        self.clusters = None
        self.method = None

    # this method checks if the cluster centers have converged to a common center. This check is done when trying
    # to find the optimal cluster center.
    def checkIfConverged(self):
        K = len(self.oldmu)
        return (set([tuple(a) for a in self.mu]) == \
                set([tuple(a) for a in self.oldmu]) \
                and len(set([tuple(a) for a in self.mu])) == K)

    def find_centers(self, method='random'):
        self.method = method
        X = self.X
        K = self.K
        self.oldmu = random.sample(X, K)
        if method != '++':
            self.mu = random.sample(X, K)
        while not self.checkIfConverged():
            self.oldmu = self.mu
            self.getClusterCenters()
            self.getNewCenters()

    def setDataPoints(self):
        my_data = genfromtxt('kmeans_data.csv', delimiter=',')
        return my_data

    def getClusterCenters(self):
        mu = self.mu
        clusters = {}
        for x in self.X:
            closestClusterCenter = min([(i[0], np.linalg.norm(x - mu[i[0]])) \
                                        for i in enumerate(mu)], key=lambda t: t[1])[0]
            try:
                clusters[closestClusterCenter].append(x)
            except KeyError:
                clusters[closestClusterCenter] = [x]
        self.clusters = clusters

    # This method finds the new cluster centers that need to be found.
    def getNewCenters(self):
        clusters = self.clusters
        newmu = []
        keys = sorted(self.clusters.keys())
        for k in keys:
            newmu.append(np.mean(clusters[k], axis=0))
        self.mu = newmu

    # This method finds the value of cost function for each K. and returns the value of cost Function for each K.

    def dist_from_centers(self):
        cent = self.mu
        X = self.X
        L2DistanceMin = np.array([min([np.linalg.norm(x - c) ** 2 for c in cent]) for x in X])
        self.D2 = L2DistanceMin
        print str(self.K) + " " + str(self.D2.sum())
        return self.K, self.D2.sum()


class KPlusPlusClustering(KMeansClustering):
    def _dist_from_centers(self):
        cent = self.mu
        X = self.X
        D2 = np.array([min([np.linalg.norm(x - c) ** 2 for c in cent]) for x in X])
        self.D2 = D2

    def estimateNextCenter(self):
        self.probabilities = self.D2 / self.D2.sum()
        self.cumalativeProbabilities = self.probabilities.cumsum()
        r = random.random()
        ind = np.where(self.cumalativeProbabilities >= r)[0][0]
        return (self.X[ind])

    def init_centers(self):
        self.mu = random.sample(self.X, 1)
        while len(self.mu) < self.K:
            self._dist_from_centers()
            self.mu.append(self.estimateNextCenter())
        print str(self.K) + " " + str(self.D2.sum())
        return self.K, self.D2.sum()

if __name__ == '__main__':

    KClusters = []
    ObjectFun = []
    for k in range(2, 11):
        kmeans = KMeansClustering(k)
        kmeans.find_centers()
        k, dist = kmeans.dist_from_centers()
        KClusters.append(k)
        ObjectFun.append(dist)
    print KClusters
    print ObjectFun
    print type(KClusters)

    Kmeanspp = []
    ObjectFunpp = []
    for k in range(2, 11):
        kplusplus = KPlusPlusClustering(k)
        k, dist = kplusplus.init_centers()
        Kmeanspp.append(k)
        ObjectFunpp.append(dist)
    print Kmeanspp
    print ObjectFunpp

    df1 = pd.DataFrame(list(zip(KClusters, ObjectFun)))
    df2 = pd.DataFrame(list(zip(Kmeanspp, ObjectFunpp)))

    df1.to_csv("KmeansGraph.csv", header=False, index=False)
    df2.to_csv("KmeansppGraph.csv", header=False, index=False)






