//Problem 1: Count and Say
//https://leetcode.com/problems/count-and-say/description/


class Solution {
    
        public String countAndSay(int n) {
		HashMap<Integer, String> rowValueMap = new HashMap<Integer, String>();
		if (n == 1) {
			return "1";
		} else if (n == 2) {
			return "11";
		} else {
			rowValueMap.put(1, "1");
			rowValueMap.put(2, "11");
			for (int i = 3; i <= n; i++) {
				rowValueMap.put(i, returnNextString(rowValueMap.get(i - 1)));
			}

		}

		return rowValueMap.get(n);

	}

	public static String returnNextString(String currentString) {
		int n = currentString.length();
		int i = 0, j = 1;
		StringBuilder resultString = new StringBuilder();
		while (i < n) {
			while ((i != (n - 1)) && (currentString.charAt(i) == currentString.charAt(i + 1))) {
				i++;
				j++;
			}
			resultString.append(j);
			resultString.append(currentString.charAt(i));
			i++;
			j = 1;
		}
		return resultString.toString();
	}
    
}


//Problem2 : Maximum Subarray
//https://leetcode.com/problems/maximum-subarray/description/


import java.lang.*;
class Solution {
    public int maxSubArray(int[] nums) {
        int maxSoFar =  nums[0];
        int maxFinal = nums[0];
        for(int i= 1;i<nums.length;i++){
            maxSoFar = Math.max(nums[i], maxSoFar+nums[i]);
            maxFinal = Math.max(maxSoFar, maxFinal);
        }
        return maxFinal;
    }
}


//Problem3 : Length of Last Word
//https://leetcode.com/problems/length-of-last-word/description/


class Solution {
    public int lengthOfLastWord(String s) {
		if (s.trim().isEmpty())
			return 0;
		String[] parts = s.split(" ");
		int n = parts.length;
		return parts[n - 1].length();
    }
}


//Problem 4 : Decode Ways
//https://leetcode.com/problems/decode-ways/description/


class Solution {
	public int numDecodings(String s) {
		int n = s.length();
		if (n == 0)
			return 0;

		int[] array = new int[n + 1];
		array[n] = 1;
		array[n - 1] = s.charAt(n - 1) != '0' ? 1 : 0;

		for (int i = n - 2; i >= 0; i--)
			if (s.charAt(i) != '0')
				array[i] = (Integer.parseInt(s.substring(i, i + 2)) <= 26) ? array[i + 1] + array[i + 2] : array[i + 1];
			else
				continue;

		return array[0];
	}
}



//Problem 5 : Triangle
//https://leetcode.com/problems/triangle/description/



class Solution {
    public int minimumTotal(List<List<Integer>> triangle) {
       
		List<Integer> currentList = triangle.get(triangle.size()-1);
		for (int i=triangle.size()-1;i>0;i--) {
			List<Integer> minList = new ArrayList<Integer>();
			for(int j=0;j<currentList.size()-1;j++) {
				minList.add(Math.min(currentList.get(j), currentList.get(j+1)));
			}
			
			currentList = triangle.get(i-1);
			for(int k=0;k<minList.size();k++) {
				currentList.set(k, currentList.get(k)+minList.get(k));
			}
		}
		return currentList.get(0);

	
    }
}
