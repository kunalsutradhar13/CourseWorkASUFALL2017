package cse512

import org.apache.spark.sql.SparkSession

object SpatialQuery extends App{
  
  def ST_Contains(x: String, y: String): Boolean = {
    var strArray: Array[String] = new Array[String](4)
    var strArray1: Array[String] = new Array[String](2)
    strArray1 = x.split(',')
    strArray = y.split(',')
    var x0: Double = strArray(0).toDouble
    var x1: Double = strArray(2).toDouble
    var y0: Double = strArray(1).toDouble
    var y1: Double = strArray(3).toDouble

    var x4: Double = strArray1(0).toDouble
    var y4: Double = strArray1(1).toDouble

    var maxx: Double = 0;
    var minx: Double = 0;
    var maxy: Double = 0;
    var miny: Double = 0;
    if (x0 > x1) {
      maxx = x0;
      minx = x1;
    } else {
      maxx = x1;
      minx = x0;
    }
    if (y0 > y1) {
      maxy = y0;
      miny = y1;
    } else {
      maxy = y1;
      miny = y0;
    }
    if (x4 >= minx && x4 <= maxx && y4 >= miny && y4 <= maxy) {
      return true
    } else {
      return false
    }

  }

  private def areaOfTriangle(x1: Double, y1: Double, x2: Double, y2: Double, x3: Double, y3: Double): Double = {
    var dist1 = distance(x1, y1, x2, y2);
    var dist2 = distance(x2, y2, x3, y3);
    var dist3 = distance(x1, y1, x3, y3);
    var s = (dist1 + dist2 + dist3) / 2;
    return Math.sqrt(s * (s - dist1) * (s - dist2) * (s - dist3));
  }

  private def distance(x1: Double, y1: Double, x2: Double, y2: Double): Double = {
    return java.lang.Math.hypot(x1 - x2, y1 - y2);
  }

  def ST_Within(x: String, y: String, dist: Double): Boolean = {
    var strArray0: Array[String] = new Array[String](2)
    var strArray1: Array[String] = new Array[String](2)

    strArray0 = x.split(',')

    var x0: Double = strArray0(0).toDouble
    var y0: Double = strArray0(1).toDouble

    strArray1 = y.split(',')
    var x1: Double = strArray1(0).toDouble
    var y1: Double = strArray1(1).toDouble

    var calDist = distance(x0, y0, x1, y1)
    if (calDist <= dist) {
      return true
    } else {
      return false
    }

  }
  
  
  def runRangeQuery(spark: SparkSession, arg1: String, arg2: String): Long = {

    val pointDf = spark.read.format("com.databricks.spark.csv").option("delimiter","\t").option("header","false").load(arg1);
    pointDf.createOrReplaceTempView("point")

    // YOU NEED TO FILL IN THIS USER DEFINED FUNCTION
    //spark.udf.register("ST_Contains",(queryRectangle:String, pointString:String)=>((true)))
    spark.udf.register("ST_Contains",(queryRectangle:String, pointString:String)=>((ST_Contains(pointString,queryRectangle))))

    val resultDf = spark.sql("select * from point where ST_Contains('"+arg2+"',point._c0)")
    resultDf.show()

    return resultDf.count()
  }

  def runRangeJoinQuery(spark: SparkSession, arg1: String, arg2: String): Long = {

    val pointDf = spark.read.format("com.databricks.spark.csv").option("delimiter","\t").option("header","false").load(arg1);
    pointDf.createOrReplaceTempView("point")

    val rectangleDf = spark.read.format("com.databricks.spark.csv").option("delimiter","\t").option("header","false").load(arg2);
    rectangleDf.createOrReplaceTempView("rectangle")

    // YOU NEED TO FILL IN THIS USER DEFINED FUNCTION
    //spark.udf.register("ST_Contains",(queryRectangle:String, pointString:String)=>((true)))
    spark.udf.register("ST_Contains",(queryRectangle:String, pointString:String)=>((ST_Contains(pointString,queryRectangle))))

    val resultDf = spark.sql("select * from rectangle,point where ST_Contains(rectangle._c0,point._c0)")
    resultDf.show()

    return resultDf.count()
  }

  def runDistanceQuery(spark: SparkSession, arg1: String, arg2: String, arg3: String): Long = {

    val pointDf = spark.read.format("com.databricks.spark.csv").option("delimiter","\t").option("header","false").load(arg1);
    pointDf.createOrReplaceTempView("point")

    // YOU NEED TO FILL IN THIS USER DEFINED FUNCTION
    //.udf.register("ST_Within",(pointString1:String, pointString2:String, distance:Double)=>((true)))
    spark.udf.register("ST_Within",(pointString1:String, pointString2:String, distance:Double)=>((ST_Within(pointString1,pointString2,distance))))

    val resultDf = spark.sql("select * from point where ST_Within(point._c0,'"+arg2+"',"+arg3+")")
    resultDf.show()

    return resultDf.count()
  }

  def runDistanceJoinQuery(spark: SparkSession, arg1: String, arg2: String, arg3: String): Long = {

    val pointDf = spark.read.format("com.databricks.spark.csv").option("delimiter","\t").option("header","false").load(arg1);
    pointDf.createOrReplaceTempView("point1")

    val pointDf2 = spark.read.format("com.databricks.spark.csv").option("delimiter","\t").option("header","false").load(arg2);
    pointDf2.createOrReplaceTempView("point2")

    // YOU NEED TO FILL IN THIS USER DEFINED FUNCTION
    //spark.udf.register("ST_Within",(pointString1:String, pointString2:String, distance:Double)=>((true)))
    spark.udf.register("ST_Within",(pointString1:String, pointString2:String, distance:Double)=>((ST_Within(pointString1,pointString2,distance))))

    val resultDf = spark.sql("select * from point1 p1, point2 p2 where ST_Within(p1._c0, p2._c0, "+arg3+")")
    resultDf.show()

    return resultDf.count()
  }
}
