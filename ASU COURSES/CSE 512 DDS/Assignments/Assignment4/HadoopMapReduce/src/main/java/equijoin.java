import java.io.IOException;
import java.util.*;

import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;

/**
 * @author Kunal Sutradhar
 *
 */
public class equijoin {

	public static String relationNamePrimary = "R";
	public static String relationNameSecondary = "S";

	public static class Map extends MapReduceBase implements Mapper<LongWritable, Text, Text, Text> {

		private Text keys = new Text();
		private Text values = new Text();

		public void map(LongWritable key, Text value, OutputCollector<Text, Text> output, Reporter reporter)
				throws IOException {

			String tupleList[] = value.toString().split(",");
			String valueSet = tupleList[0];
			String keyjoins = tupleList[1];
			for (int i = 1; i < tupleList.length; i++) {
				valueSet = valueSet + "," + tupleList[i];
			}
			keys.set(keyjoins);
			values.set(valueSet);

			output.collect(keys, values);
		}
	}

	public static class Reduce extends MapReduceBase implements Reducer<Text, Text, Text, Text> {
		public void reduce(Text key, Iterator<Text> values, OutputCollector<Text, Text> output, Reporter reporter)
				throws IOException {

			List<String> RelationR = new ArrayList<String>();
			List<String> RelationS = new ArrayList<String>();

			Text solTest = new Text();
			String resultTuple = "";

			while (values.hasNext()) {
				String temp = values.next().toString();
				String Split[] = temp.split(",");

				if (Split[0].equals(relationNameSecondary)) {
					RelationS.add(temp);
				} else if (Split[0].equals(relationNamePrimary)) {
					RelationR.add(temp);
				}
			}

			if (RelationS.size() == 0 || RelationR.size() == 0) {
				key.clear();
			} else {
				for (int i = 0; i < RelationR.size(); i++) {
					for (int j = 0; j < RelationS.size(); j++) {
						resultTuple = RelationR.get(i) + "," + RelationS.get(j);
						solTest.set(resultTuple);
						output.collect(new Text(""), solTest);
					}
				}
			}
		}
	}

	public JobConf setJobConf(JobConf conf, String[] args) {
		conf.setJobName("EquiJoinThroughMapReduce");
		conf.setOutputKeyClass(Text.class);
		conf.setOutputValueClass(Text.class);
		conf.set("mapred.textoutputformat.separator", " ");
		conf.setMapperClass(Map.class);
		conf.setReducerClass(Reduce.class);
		FileInputFormat.setInputPaths(conf, new Path(args[0]));
		FileOutputFormat.setOutputPath(conf, new Path(args[1]));
		return conf;
	}

	public static void main(String[] args) throws Exception {
		equijoin ej = new equijoin();
		JobConf conf = new JobConf(equijoin.class);
		ej.setJobConf(conf, args);
		JobClient.runJob(conf);
	}
}