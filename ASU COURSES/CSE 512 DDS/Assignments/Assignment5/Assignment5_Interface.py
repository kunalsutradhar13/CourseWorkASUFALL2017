#!/usr/bin/python2.7
#
# Assignment3 Interface
# Name:
#

from pymongo import MongoClient
import os
import sys
import json
import math
import re


def FindBusinessBasedOnCity(cityToSearch, saveLocation1, collection):
    outputData = collection.find({"city": re.compile(cityToSearch, re.IGNORECASE)})
    targetLocation = open(saveLocation1, 'w')
    for item in outputData:
        targetLocation.write(
            item["name"].upper().encode('utf-8').encode('string_escape') + "$" + item["full_address"].replace('\n', ',').upper().encode('utf-8').encode(
                'string_escape') + "$" + item["city"].upper().encode('utf-8').encode('string_escape') + "$" + item[
                "state"].upper().encode('utf-8').encode('string_escape'))
        targetLocation.write("\n")
    targetLocation.close()


def FindBusinessBasedOnLocation(categoriesToSearch, myLocation, maxDistance, saveLocation2, collection):
    outputContent = collection.find()
    targetFile = open(saveLocation2, 'w')
    myLatitude = float(myLocation[0])
    myLongitude = float(myLocation[1])
    finalList = []
    for item in outputContent:
        categories = item["categories"]
        lat = item["latitude"]
        lon = item["longitude"]
        categories = [it.upper() for it in categories]
        categoriesToSearch = [it.upper() for it in categoriesToSearch]
        categoriesSet = set(categories)
        categoriesToSearchSet = set(categoriesToSearch)
        #print categoriesSet - categoriesToSearchSet
        if (maxDistance >= distance(lat, lon, myLatitude, myLongitude) and len(categoriesToSearchSet- categoriesSet)==0):
            finalList.append(item["name"].upper().encode('utf-8'))
    for item in finalList:
        targetFile.write(item)
        targetFile.write("\n")
    targetFile.close()


def distance(lat2, lon2, lat1, lon1):
    phi1 = math.radians(lat1)
    phi2 = math.radians(lat2)
    delphi = math.radians(lat2 - lat1)
    dellambda = math.radians(lon2 - lon1)

    a = math.sin(delphi / 2) * math.sin(delphi / 2) + math.cos(phi1) * math.cos(phi2) * \
                                                      math.sin(dellambda / 2) * math.sin(dellambda / 2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    return 3959 * c
