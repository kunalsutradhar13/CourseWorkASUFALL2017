#!/usr/bin/python2.7
#
# Interface for the assignement
#

import psycopg2

DATABASE_NAME = 'dds_assgn1'


def getopenconnection(user='postgres', password='1234', dbname='dds_assgn1'):
    return psycopg2.connect("dbname='" + dbname + "' user='" + user + "' host='localhost' password='" + password + "'")


def loadratings(ratingstablename, ratingsfilepath, openconnection):
    cur = openconnection.cursor()
    try:
        # cur.execute("DROP TABLE IF EXISTS ratings")
        cur.execute("CREATE TABLE ratings(userid INTEGER, movieid INTEGER, rating NUMERIC)")
        row = []
        with open(ratingsfilepath) as myfile:
            j = 0
            for item in myfile:
                # print item
                temp = item.split("::")
                row.append([temp[0], temp[1], temp[2]])
                # print row
                j = j + 1

            #print j
            query = "INSERT INTO ratings(userid, movieid, rating) VALUES"
            for i in xrange(0, j):
                if(i != j-1):
                    query = query + " ( " + str(row[i][0]) + " , " + str(row[i][1]) + " , " + str(row[i][2]) + " ) , "
                else:
                    query = query + " ( " + str(row[i][0]) + " , " + str(row[i][1]) + " , " + str(row[i][2]) + " ) "

            #print query
            cur.execute(query, row)
        openconnection.commit()
    except psycopg2.DatabaseError, e:
        if openconnection:
            openconnection.rollback()
        print 'Error %s' % e
    finally:
        pass


def rangepartition(ratingstablename, numberofpartitions, openconnection):
    cur = openconnection.cursor()
    # print type(numberofpartitions)
    try:
        #Making partitions
        if numberofpartitions< 0 or numberofpartitions>5:
            print "****Nothing wrong with the code!! The issue is with number of partitions, please enter valid numberofpartitions for insert to work****\n "
        else :
            range = 5.0 / numberofpartitions
            floatrange = float(range)
            init = 0.0
            final = range
            for i in xrange(0, numberofpartitions):
                if i == 0:
                    query = "CREATE TABLE " \
                            "range_part" + str(i) + "(CHECK(rating >=" + str(init) + " AND rating <=" + str(
                        final) + ")) INHERITS(ratings);"
                else:
                    query = "CREATE TABLE " \
                            "range_part" + str(i) + "(CHECK(rating >" + str(init) + " AND rating <=" + str(
                        final) + ")) INHERITS(ratings);"
                #print query
                cur.execute(query)
                init = float(final)
                final = float(init + floatrange)


            #Adding already existing rows to child tables.
            init = 0.0
            final = range
            for i in xrange(0, numberofpartitions):
                if i == 0:
                    insertquery = "INSERT INTO range_part" + str(i) + "(userid, movieid, rating) " \
                                                                    "SELECT userid, movieid, rating from ratings where rating >= " \
                                  + str(init) + "AND rating <= " + str(final) + ";\n"
                else:
                    insertquery = "INSERT INTO range_part" + str(i) + "(userid, movieid, rating)" \
                                                                    " SELECT userid, movieid, rating from ratings where rating > " \
                                  + str(init) + "AND rating <= " + str(final) + ";\n"
                #print insertquery
                cur.execute(insertquery)
                init = float(final)
                final = float(init + floatrange)


            # Writing the trigger query for the newly partitioned tables.
            init = 0.0
            final = range
            triggerQuery = "CREATE OR REPLACE FUNCTION ratings_insert_trigger() " \
                           "\nRETURNS TRIGGER AS $$ \n" \
                           "BEGIN\n"
            for i in xrange(0, numberofpartitions):
                if i == 0:
                    triggerQuery = triggerQuery + "IF ( NEW.rating >=" + str(init) + \
                                   " AND NEW.rating <=" + str(final) + " ) THEN " \
                                                                            "INSERT INTO range_part" + str(
                        i) + " VALUES (NEW.*);\n"
                else:
                    triggerQuery = triggerQuery + "ELSIF ( NEW.rating >" + str(init) + \
                                   " AND NEW.rating <=" + str(final) + " ) THEN " \
                                                                            "INSERT INTO range_part" + str(
                        i) + " VALUES (NEW.*);\n"
                init = final
                final = init + range
            triggerQuery = triggerQuery + "ELSE \n" \
                                          "RAISE EXCEPTION 'Value out of range.  Fix the measurement_insert_trigger() function!';\n" \
                                          "END IF;\n" \
                                          "RETURN NULL;\n" \
                                          "END;\n" \
                                          "$$\n" \
                                          "LANGUAGE plpgsql;\n"
            #print triggerQuery
            cur.execute(triggerQuery)

            # writing the trigger invocation for the newly partitoned tables.
            triggerQuery2 = "CREATE TRIGGER trigger_ratings_insert \n" \
                            "BEFORE INSERT ON ratings \n" \
                            "FOR EACH ROW EXECUTE PROCEDURE ratings_insert_trigger();\n"
            #print triggerQuery2
            cur.execute(triggerQuery2)

            openconnection.commit()
    except psycopg2.DatabaseError, e:
        if openconnection:
            openconnection.rollback()
        print 'Error %s' % e
    finally:
        pass


def roundrobinpartition(ratingstablename, numberofpartitions, openconnection):
    cur = openconnection.cursor()

    # print type(numberofpartitions)
    try:

        if numberofpartitions < 0 or numberofpartitions > 5:
            print "****Nothing wrong with the code!! The issue is with number of partitions, please enter valid number of partitions for insert to work****\n"
        else:
            # Making partitions

            for i in xrange(0, numberofpartitions):
                query = "CREATE TABLE " \
                        "rrobin_part" + str(i) + "() INHERITS(ratings);"
                #print query
                cur.execute(query)


            # Adding already existing rows to child tables.

            countQuery = "SELECT COUNT(*) FROM only ratings"
            cur.execute(countQuery)
            count = cur.fetchone()[0]
            #print str(count) + " before rr partition"
            fetchQuery = "SELECT * FROM only ratings"
            cur.execute(fetchQuery)
            entries = cur.fetchall()
            m = 0
            for i in xrange(0,count):
                insertquery = "INSERT INTO rrobin_part" + str(m%numberofpartitions) + "(userid, movieid, rating) "\
                            "VALUES (" + str(entries[i][0]) + "," +  str(entries[i][1]) + "," +  str(entries[i][2]) +")"
                cur.execute(insertquery)
                m = m+1


            cur.execute("CREATE TABLE roundrobinvalue(rrvalue INTEGER, numberofpartitions INTEGER)")
            cur.execute("Insert into roundrobinvalue(rrvalue, numberofpartitions) VALUES(" + str(m%numberofpartitions) +" , "+ str(numberofpartitions)+")")

            openconnection.commit()
    except psycopg2.DatabaseError, e:
        if openconnection:
            openconnection.rollback()
        print 'Error %s' % e
    finally:
        pass


def roundrobininsert(ratingstablename, userid, itemid, rating, openconnection):
    cur = openconnection.cursor()
    try:
        fetchQuery = "SELECT * FROM roundrobinvalue"
        cur.execute(fetchQuery)
        entry = cur.fetchone()
        roundRobinValue = entry[0]
        #print roundRobinValue
        numberOfPartitions = entry[1]
        #print numberOfPartitions
        insertquery = "INSERT INTO rrobin_part" + str(
            roundRobinValue % numberOfPartitions) + "(userid, movieid, rating) " \
                                                    "VALUES (" + str(userid) + "," + str(itemid) + "," + str(rating) + ")"
        print insertquery
        cur.execute(insertquery)

        roundRobinValue = roundRobinValue + 1
        updateQuery = "UPDATE roundrobinvalue SET rrvalue = " + str(
            roundRobinValue) + " WHERE numberofpartitions = " + str(numberOfPartitions) + ";"
        #print updateQuery
        cur.execute(updateQuery)
        openconnection.commit()
    except psycopg2.DatabaseError, e:
        if openconnection:
            openconnection.rollback()
        print 'Error %s' % e
    finally:
        pass


def rangeinsert(ratingstablename, userid, itemid, rating, openconnection):
    cur = openconnection.cursor()
    try:
        insertQuery = "INSERT INTO "+ str(ratingstablename) + "( userid, movieid, rating ) VALUES ( " + str(userid) + " , " + str(itemid) + " , " + str(rating) + ")"
        print insertQuery
        cur.execute(insertQuery)
        openconnection.commit()
    except psycopg2.DatabaseError, e:
        if openconnection:
            openconnection.rollback()
        print 'Error %s' % e
    finally:
        pass


def deletepartitionsandexit(openconnection):
    cur = openconnection.cursor()
    try:
        query = "select relname from pg_inherits i join pg_class c on c.oid = inhrelid " \
                "where inhparent = 'ratings'::regclass"

        # print query
        cur.execute(query)
        partitionList = cur.fetchall()
        # print partitionList


        query = "select count(relname) from pg_inherits i join pg_class c on c.oid = inhrelid " \
                "where inhparent = 'ratings'::regclass"
        # print query
        cur.execute(query)

        count = cur.fetchone()
        # print int(count[0])

        for i in xrange(0, int(count[0])):
            # deleteQuery = "ALTER TABLE " + ratingstablename + " DROP PARTITION " + partitionList[i][0]
            deleteQuery = "DROP TABLE " + partitionList[i][0]
            # print deleteQuery
            cur.execute(deleteQuery)
        deleteMaster = "Drop table ratings"
        # print deleteMaster
        cur.execute(deleteMaster)
        cur.execute("DROP TABLE IF EXISTS roundrobinvalue")
        cur.execute("DROP FUNCTION IF EXISTS ratings_insert_trigger_rr()")
        cur.execute("DROP FUNCTION IF EXISTS ratings_insert_trigger()")

        openconnection.commit()
    except psycopg2.DatabaseError, e:
        if openconnection:
            openconnection.rollback()
        print 'Error %s' % e
    finally:
        pass

def create_db(dbname):
    """
    We create a DB by connecting to the default user and database of Postgres
    The function first checks if an existing database exists for a given name, else creates it.
    :return:None
    """
    # Connect to the default database
    con = getopenconnection(dbname='postgres')
    con.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    cur = con.cursor()

    # Check if an existing database with the same name exists
    cur.execute('SELECT COUNT(*) FROM pg_catalog.pg_database WHERE datname=\'%s\'' % (dbname,))
    count = cur.fetchone()[0]
    if count == 0:
        cur.execute('CREATE DATABASE %s' % (dbname,))  # Create the database
    else:
        print 'A database named {0} already exists'.format(dbname)

    # Clean up
    cur.close()
    con.close()


# Middleware
def before_db_creation_middleware():
    # Use it if you want to
    pass


def after_db_creation_middleware(databasename):
    # Use it if you want to
    pass


def before_test_script_starts_middleware(openconnection, databasename):
    # Use it if you want to
    pass


def after_test_script_ends_middleware(openconnection, databasename):
    # Use it if you want to
    pass


if __name__ == '__main__':
    try:

        # Use this function to do any set up before creating the DB, if any
        before_db_creation_middleware()

        create_db(DATABASE_NAME)

        # Use this function to do any set up after creating the DB, if any
        after_db_creation_middleware(DATABASE_NAME)

        with getopenconnection() as con:
            # Use this function to do any set up before I starting calling your functions to test, if you want to
            before_test_script_starts_middleware(con, DATABASE_NAME)

            # Here is where I will start calling your functions to test them. For example,
            #loadratings('ratings.dat', con)
            # ###################################################################################
            # Anything in this area will not be executed as I will call your functions directly
            # so please add whatever code you want to add in main, in the middleware functions provided "only"
            # ###################################################################################

            # Use this function to do any set up after I finish testing, if you want to
            after_test_script_ends_middleware(con, DATABASE_NAME)

    except Exception as detail:
        print "OOPS! This is the error ==> ", detail