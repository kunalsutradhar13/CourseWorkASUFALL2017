#!/usr/bin/python2.7
#
# Assignment2 Interface
#

import psycopg2
import os
import sys
# Donot close the connection inside this file i.e. do not perform openconnection.close()
def RangeQuery(ratingsTableName, ratingMinValue, ratingMaxValue, openconnection):
    rangeName = "rangeratingspart"
    roundName = "roundrobinratingspart"
    pwd = os.getcwd()
    f= open("RangeQueryOut.txt","w+")
    f.close()	
    rangeOutputFile = pwd+"/RangeQueryOut.txt"
    os.chmod(rangeOutputFile, 0o777)
    rangeQuery1 = "COPY ("
    rangeQuery2 = ""
    rangeQuery3 = ") To '"+rangeOutputFile+"' WITH CSV;"
    try:
        cursor = openconnection.cursor()

	# get Range Entries

	cursor.execute("SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE '%"+rangeName+"%' ")
	count = cursor.rowcount
        #print count
	if not bool(count):
		print "Range table partitions dont exist"
	else :
		i=1;
		rows = cursor.fetchall()
		for row in rows:
			if(i<count):
				rangeQuery2 = rangeQuery2 + "SELECT '"+row[0]+"' as TableName, userid, movieid, rating FROM "+row[0]+" where rating>="+str(float(ratingMinValue))+" and rating<="+str(float(ratingMaxValue)) + " UNION ALL "
				i= i+1
			elif(i==count):
				rangeQuery2 = rangeQuery2 + "SELECT '"+row[0]+"' as TableName, userid, movieid, rating FROM "+row[0]+" where rating>="+str(float(ratingMinValue))+" and rating<="+str(float(ratingMaxValue)) + " UNION ALL "
	#print rangeQuery1 + rangeQuery2 + rangeQuery3		
	
	# Get Round Robin entries.

	cursor.execute("SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE '%"+roundName+"%' ")
	count = cursor.rowcount
        #print count
	if not bool(count):
		print "Round Robin table partitions dont exist"
	else :
		i=1;
		rows = cursor.fetchall()
		for row in rows:
			if(i<count):
				rangeQuery2 = rangeQuery2 + "SELECT '"+row[0]+"' as TableName, userid, movieid, rating FROM "+row[0]+" where rating>="+str(float(ratingMinValue))+" and rating<="+str(float(ratingMaxValue)) + " UNION ALL "
				i= i+1
			elif(i==count):
				rangeQuery2 = rangeQuery2 + "SELECT '"+row[0]+"' as TableName, userid, movieid, rating FROM "+row[0]+" where rating>="+str(float(ratingMinValue))+" and rating<="+str(float(ratingMaxValue))
	print rangeQuery1 + rangeQuery2 + rangeQuery3	

	
	cursor.execute(rangeQuery1 + rangeQuery2 + rangeQuery3)

    except psycopg2.DatabaseError, e:
        if openconnection:
            openconnection.rollback()
        print 'Error %s' % e
        sys.exit(1)
    except IOError, e:
        if openconnection:
            openconnection.rollback()
        print 'Error %s' % e
        sys.exit(1)
    finally:
        if cursor:
            cursor.close()


def PointQuery(ratingsTableName, ratingValue, openconnection):
    #Implement PointQuery Here. PointQueryOut.txt
    rangeName = "rangeratingspart"
    roundName = "roundrobinratingspart"
    pwd = os.getcwd()
    f= open("PointQueryOut.txt","w+")
    f.close()	
    rangeOutputFile = pwd+"/PointQueryOut.txt"
    os.chmod(rangeOutputFile, 0o777)
    rangeQuery1 = "COPY ("
    rangeQuery2 = ""
    rangeQuery3 = ") To '"+rangeOutputFile+"' WITH CSV;"
    try:
        cursor = openconnection.cursor()

	# get Range Entries

	cursor.execute("SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE '%"+rangeName+"%' ")
	count = cursor.rowcount
        #print count
	if not bool(count):
		print "Range table partitions dont exist"
	else :
		i=1;
		rows = cursor.fetchall()
		for row in rows:
			if(i<count):
				rangeQuery2 = rangeQuery2 + "SELECT '"+row[0]+"' as TableName, userid, movieid, rating FROM "+row[0]+" where rating="+str(float(ratingValue))+" UNION ALL "
				i= i+1
			elif(i==count):
				rangeQuery2 = rangeQuery2 + "SELECT '"+row[0]+"' as TableName, userid, movieid, rating FROM "+row[0]+" where rating="+str(float(ratingValue))+" UNION ALL "
	#print rangeQuery1 + rangeQuery2 + rangeQuery3		
	
	# Get Round Robin entries.

	cursor.execute("SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME LIKE '%"+roundName+"%' ")
	count = cursor.rowcount
        #print count
	if not bool(count):
		print "Round Robin table partitions dont exist"
	else :
		i=1;
		rows = cursor.fetchall()
		for row in rows:
			if(i<count):
				rangeQuery2 = rangeQuery2 + "SELECT '"+row[0]+"' as TableName, userid, movieid, rating FROM "+row[0]+" where rating="+str(float(ratingValue))+" UNION ALL "
				i= i+1
			elif(i==count):
				rangeQuery2 = rangeQuery2 + "SELECT '"+row[0]+"' as TableName, userid, movieid, rating FROM "+row[0]+" where rating="+str(float(ratingValue))
	print rangeQuery1 + rangeQuery2 + rangeQuery3	

	cursor.execute(rangeQuery1 + rangeQuery2 + rangeQuery3)

    except psycopg2.DatabaseError, e:
        if openconnection:
            openconnection.rollback()
        print 'Error %s' % e
        sys.exit(1)
    except IOError, e:
        if openconnection:
            openconnection.rollback()
        print 'Error %s' % e
        sys.exit(1)
    finally:
        if cursor:
            cursor.close()
