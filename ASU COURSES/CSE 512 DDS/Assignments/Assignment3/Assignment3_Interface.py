#!/usr/bin/python2.7
#
# Assignment3 Interface
#

import psycopg2
import os
import sys
import threading

##################### This needs to changed based on what kind of table we want to sort. ##################
##################### To know how to change this, see Assignment 3 Instructions carefully #################
FIRST_TABLE_NAME = 'table1'
SECOND_TABLE_NAME = 'table2'
SORT_COLUMN_NAME_FIRST_TABLE = 'column1'
SORT_COLUMN_NAME_SECOND_TABLE = 'column2'
JOIN_COLUMN_NAME_FIRST_TABLE = 'column1'
JOIN_COLUMN_NAME_SECOND_TABLE = 'column2'
ThreadCount = 5
##########################################################################################################


# Donot close the connection inside this file i.e. do not perform openconnection.close()
def ParallelSort (InputTable, SortingColumnName, OutputTable, openconnection):
    # Implement ParallelSort Here.
    cursor = openconnection.cursor()
    #print InputTable
    query = "SELECT MIN(" + SortingColumnName + ") FROM " + InputTable
    cursor.execute(query)
    minVal = cursor.fetchone()[0]
    #print minVal

    query = "SELECT MAX(" + SortingColumnName + ") FROM " + InputTable
    cursor.execute(query)
    maxVal = cursor.fetchone()[0]
    #print maxVal

    query = "SELECT column_name, data_type FROM information_schema.columns WHERE table_name = \'" + InputTable + "\';"
    cursor.execute(query)
    inputTableSchema = cursor.fetchall()

    rangepartition(InputTable, ThreadCount, SortingColumnName, minVal, maxVal, openconnection)

    #print "created all rangeValue partitions"

    threads = [0, 0, 0, 0, 0]
    rangeValue = maxVal - minVal / ThreadCount
    floatrange = float(rangeValue)
    init = 0.0
    final = float(rangeValue)
    for i in range(ThreadCount):
        threads[i] = threading.Thread(target=insertIntoPartitions,
                                      args=(init, final, i, InputTable, SortingColumnName, openconnection))
        threads[i].start()
        init = float(final)
        final = float(init + floatrange)

    for i in range(ThreadCount):
        threads[i].join()

    createTable(OutputTable, inputTableSchema, openconnection)

    for i in range(ThreadCount):
        query = "INSERT INTO " + OutputTable + " SELECT * FROM rangePart" + str(i) + ";"
        cursor.execute(query)

    for i in range(ThreadCount):
        query = "DROP TABLE IF EXISTS rangePart" + str(i) + ";"
        cursor.execute(query)

    openconnection.commit()


def rangepartition(ratingstablename, numberofpartitions, SortingColumnName, minVal, MaxVal, openconnection):
    cur = openconnection.cursor()
    if numberofpartitions < 0 or numberofpartitions > 5:
        print "****Nothing wrong with the code!! The issue is with number of partitions, please enter valid numberofpartitions for insert to work****\n "
    else:
        range = MaxVal - minVal / numberofpartitions
        floatrange = float(range)
        init = 0.0
        final = range
        for i in xrange(0, numberofpartitions):
            if i == 0:
                query = "CREATE TABLE " \
                        "rangePart" + str(i) + "(CHECK(" + SortingColumnName + " >=" + str(
                    init) + " AND " + SortingColumnName + " <=" + str(
                    final) + ")) INHERITS(" + ratingstablename + ");"
            else:
                query = "CREATE TABLE " \
                        "rangePart" + str(i) + "(CHECK(" + SortingColumnName + " >" + str(
                    init) + " AND " + SortingColumnName + " <=" + str(
                    final) + ")) INHERITS(" + ratingstablename + ");"
            #print query
            cur.execute(query)
            init = float(final)
            final = float(init + floatrange)
    openconnection.commit()


def insertIntoPartitions(init, final, i, ratingsTableName, SortingColumnName, openconnection):
    cur = openconnection.cursor()
    if i == 0:
        insertquery = "INSERT INTO rangePart" + str(
            i) + " select * from " + ratingsTableName + " where " + SortingColumnName + " >= " \
                      + str(init) + " AND " + SortingColumnName + " <= " + str(
            final) + " ORDER BY " + SortingColumnName + " ASC;\n"
    else:
        insertquery = "INSERT INTO rangePart" + str(
            i) + " select * from " + ratingsTableName + " where " + SortingColumnName + " > " \
                      + str(init) + " AND " + SortingColumnName + " <= " + str(
            final) + " ORDER BY " + SortingColumnName + " ASC;\n"
    #print insertquery
    cur.execute(insertquery)
    openconnection.commit()


def createTable(tableName, inputTableSchema, openconnection):
    cursor = openconnection.cursor()

    query = "DROP TABLE IF EXISTS "+tableName+";"
    cursor.execute(query)

    query = "CREATE TABLE "+tableName+" ("+inputTableSchema[0][0]+" "+inputTableSchema[0][1]+");"
    cursor.execute(query)

    for j in range(1, len(inputTableSchema)):
        query = "ALTER TABLE "+tableName+" ADD COLUMN "+inputTableSchema[j][0] +" "+inputTableSchema[j][1]+";"
        cursor.execute(query)
        
def getInputSchema(InputTable, openconnection):
    cursor = openconnection.cursor()
    query = "SELECT column_name, data_type FROM information_schema.columns WHERE table_name = \'"+InputTable+"\';"
    cursor.execute(query)
    inputTableSchema = cursor.fetchall()
    return inputTableSchema

def ParallelJoin (InputTable1, InputTable2, Table1JoinColumn, Table2JoinColumn, OutputTable, openconnection):
    cursor = openconnection.cursor()
    minValueTable1 = getMinValue(Table1JoinColumn, InputTable1, openconnection)
    minValueTable2 = getMinValue(Table2JoinColumn, InputTable2, openconnection)
    maxValueTable1 = getMaxValue(Table1JoinColumn, InputTable1, openconnection)
    maxValueTable2 = getMaxValue(Table2JoinColumn, InputTable2, openconnection)

    maxValue = max(maxValueTable1, maxValueTable2)
    minValue = min(minValueTable1, minValueTable2)
    interval = (maxValue - minValue) / ThreadCount

    inputSchemaTable1 = getInputSchema(InputTable1, openconnection)
    inputSchemaTable2 = getInputSchema(InputTable2, openconnection)

    query = "DROP TABLE IF EXISTS " + OutputTable + ";"
    cursor.execute(query)
    query = "CREATE TABLE " + OutputTable + " (" + inputSchemaTable1[0][0] + " " + inputSchemaTable2[0][1] + ");"
    cursor.execute(query)
    for j in range(1, len(inputSchemaTable1)):
        query = "ALTER TABLE " + OutputTable + " ADD COLUMN " + inputSchemaTable1[j][0] + " " + inputSchemaTable1[j][
            1] + ";"
        cursor.execute(query)
    for j in range(len(inputSchemaTable2)):
        query = "ALTER TABLE " + OutputTable + " ADD COLUMN " + inputSchemaTable2[j][0] + " " + inputSchemaTable2[j][
            1] + ";"
        cursor.execute(query)

    makeRangePartitionForTable(InputTable1, Table1JoinColumn, interval, minValue, "input1RangeTable", openconnection)
    makeRangePartitionForTable(InputTable2, Table2JoinColumn, interval, minValue, "input2RangeTable", openconnection)

    for i in range(ThreadCount):
        outputRangeTableName = "outputRangeTable" + str(i)
        query = "DROP TABLE IF EXISTS " + outputRangeTableName + ";"
        cursor.execute(query)
        query = "CREATE TABLE " + outputRangeTableName + " ( " + inputSchemaTable1[0][0] + " " + inputSchemaTable2[0][
            1] + ");"
        cursor.execute(query, query)
        for j in range(1, len(inputSchemaTable1)):
            query = "ALTER TABLE " + outputRangeTableName + " ADD COLUMN " + inputSchemaTable1[j][0] + " " + \
                    inputSchemaTable1[j][1] + ";"
            cursor.execute(query)
        for j in range(len(inputSchemaTable2)):
            query = "ALTER TABLE " + outputRangeTableName + " ADD COLUMN " + inputSchemaTable2[j][0] + " " + \
                    inputSchemaTable2[j][1] + ";"
            cursor.execute(query)

    threads = [0, 0, 0, 0, 0]
    for i in range(ThreadCount):
        threads[i] = threading.Thread(target=joinInsertParallel,
                                      args=(Table1JoinColumn, Table2JoinColumn, i, openconnection))
        threads[i].start()

    for i in range(ThreadCount):
        threads[i].join()

    for i in range(ThreadCount):
        query = "INSERT INTO " + OutputTable + " SELECT * FROM outputRangeTable" + str(i) + ";"
        cursor.execute(query)

    for i in range(ThreadCount):
        query1 = "DROP TABLE IF EXISTS input1RangeTable" + str(i) + ";"
        query2 = "DROP TABLE IF EXISTS input2RangeTable" + str(i) + ";"
        query3 = "DROP TABLE IF EXISTS outputRangeTable" + str(i) + ";"
        cursor.execute(query1)
        cursor.execute(query2)
        cursor.execute(query3)

    openconnection.commit()



def getMinValue(Column, Table, openconnection):
    cursor = openconnection.cursor()
    query = "SELECT MIN(" + Column + ") FROM " + Table + ";"
    cursor.execute(query)
    result = cursor.fetchone()
    minValueTable1 = float(result[0])
    return minValueTable1


def getMaxValue(Column, Table, openconnection):
    cursor = openconnection.cursor()
    query = "SELECT MAX(" + Column + ") FROM " + Table + ";"
    cursor.execute(query)
    result = cursor.fetchone()
    minValueTable1 = float(result[0])
    return minValueTable1

def joinInsertParallel(Table1JoinColumn, Table2JoinColumn, i, openconnection):
    cursor = openconnection.cursor()
    query = "INSERT INTO outputRangeTable" + str(i) + " SELECT * FROM " \
            "input1RangeTable" + str(i) + " INNER JOIN input2RangeTable" + str(i) + " " \
            "ON input1RangeTable" + str(i) + "." + Table1JoinColumn + " = input2RangeTable" + str(i) + "." + Table2JoinColumn + ";"
    cursor.execute(query)
    return


def makeRangePartitionForTable(InputTable, TableJoinColumn, interval, minValue, temporaryTable, openconnection):
    cursor = openconnection.cursor()
    for i in range(ThreadCount):
        tableName = temporaryTable + str(i)
        query = "DROP TABLE IF EXISTS " + tableName + ";"
        cursor.execute(query)
        if i == 0:
            min = minValue
            max = minValue + interval
            query = "CREATE TABLE " + tableName + " AS " \
                    "SELECT * FROM " + InputTable + " " \
                    "WHERE " + TableJoinColumn + " >= " + str(min) + " AND " + TableJoinColumn + " <= " + str(max) + ";"
        else:
            min = max
            max += interval
            query = "CREATE TABLE " + tableName + " AS " \
                    "SELECT * FROM " + InputTable + " " \
                    "WHERE " + TableJoinColumn + " >= " + str(min) + " AND " + TableJoinColumn + " <= " + str(max) + ";"

        cursor.execute(query)



################### DO NOT CHANGE ANYTHING BELOW THIS #############################


# Donot change this function
def getOpenConnection(user='postgres', password='1234', dbname='ddsassignment3'):
    return psycopg2.connect("dbname='" + dbname + "' user='" + user + "' host='localhost' password='" + password + "'")

# Donot change this function
def createDB(dbname='ddsassignment3'):
    """
    We create a DB by connecting to the default user and database of Postgres
    The function first checks if an existing database exists for a given name, else creates it.
    :return:None
    """
    # Connect to the default database
    con = getOpenConnection(dbname='postgres')
    con.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
    cur = con.cursor()

    # Check if an existing database with the same name exists
    cur.execute('SELECT COUNT(*) FROM pg_catalog.pg_database WHERE datname=\'%s\'' % (dbname,))
    count = cur.fetchone()[0]
    if count == 0:
        cur.execute('CREATE DATABASE %s' % (dbname,))  # Create the database
    else:
        print 'A database named {0} already exists'.format(dbname)

    # Clean up
    cur.close()
    con.commit()
    con.close()

# Donot change this function
def deleteTables(ratingstablename, openconnection):
    try:
        cursor = openconnection.cursor()
        if ratingstablename.upper() == 'ALL':
            cursor.execute("SELECT table_name FROM information_schema.tables WHERE table_schema = 'public'")
            tables = cursor.fetchall()
            for table_name in tables:
                cursor.execute('DROP TABLE %s CASCADE' % (table_name[0]))
        else:
            cursor.execute('DROP TABLE %s CASCADE' % (ratingstablename))
        openconnection.commit()
    except psycopg2.DatabaseError, e:
        if openconnection:
            openconnection.rollback()
        print 'Error %s' % e
        sys.exit(1)
    except IOError, e:
        if openconnection:
            openconnection.rollback()
        print 'Error %s' % e
        sys.exit(1)
    finally:
        if cursor:
            cursor.close()

# Donot change this function
def saveTable(ratingstablename, fileName, openconnection):
    try:
        cursor = openconnection.cursor()
        cursor.execute("Select * from %s" %(ratingstablename))
        data = cursor.fetchall()
        openFile = open(fileName, "w")
        for row in data:
            for d in row:
                openFile.write(`d`+",")
            openFile.write('\n')
        openFile.close()
    except psycopg2.DatabaseError, e:
        if openconnection:
            openconnection.rollback()
        print 'Error %s' % e
        sys.exit(1)
    except IOError, e:
        if openconnection:
            openconnection.rollback()
        print 'Error %s' % e
        sys.exit(1)
    finally:
        if cursor:
            cursor.close()

if __name__ == '__main__':
    try:
	# Creating Database ddsassignment3
	print "Creating Database named as ddsassignment3"
	createDB();
	
	# Getting connection to the database
	print "Getting connection from the ddsassignment3 database"
	con = getOpenConnection();

	# Calling ParallelSort
	print "Performing Parallel Sort"
	ParallelSort(FIRST_TABLE_NAME, SORT_COLUMN_NAME_FIRST_TABLE, 'parallelSortOutputTable', con);

	# Calling ParallelJoin
	print "Performing Parallel Join"
	ParallelJoin(FIRST_TABLE_NAME, SECOND_TABLE_NAME, JOIN_COLUMN_NAME_FIRST_TABLE, JOIN_COLUMN_NAME_SECOND_TABLE, 'parallelJoinOutputTable', con);
	
	# Saving parallelSortOutputTable and parallelJoinOutputTable on two files
	saveTable('parallelSortOutputTable', 'parallelSortOutputTable.txt', con);
	saveTable('parallelJoinOutputTable', 'parallelJoinOutputTable.txt', con);

	# Deleting parallelSortOutputTable and parallelJoinOutputTable
	deleteTables('parallelSortOutputTable', con);
       	deleteTables('parallelJoinOutputTable', con);

        if con:
            con.close()

    except Exception as detail:
        print "Something bad has happened!!! This is the error ==> ", detail
